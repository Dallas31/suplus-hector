#include "form_fournisseur.h"
#include "ui_form_fournisseur.h"

Form_fournisseur::Form_fournisseur(QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_fournisseur)
{
    ui->setupUi(this);
    this->setWindowTitle ("Ajout nouveau fournisseur");

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT( envoyerMessage(QString,bool) ) );

    ui->lineEdit_nomFournisseur->setAlignment ( Qt::AlignCenter );

    if( contexte != "modifier" ){

        ui->btn_modifier->setEnabled ( false );
        ui->lineEdit_nouveauFrs->setEnabled ( false );
        ui->label_nouveauFrs->setEnabled ( false );
    }
    else{

        ui->btn_valider->setEnabled ( false );
        ui->label_fournisseurActuel->setEnabled ( false );
        ui->comboBox_fournisseur->setEnabled ( false );

        ui->lineEdit_nouveauFrs->setAlignment ( Qt::AlignCenter );
    }

    listerFournisseur ();

}

Form_fournisseur::~Form_fournisseur()
{
    delete ui;
}







//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::listerFournisseur(){

    ui->comboBox_fournisseur->addItems ( bdd.listerValeur ( "tbl_fournisseur", "nomFournisseur") );
}







//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::chargerFournisseur(QString nomFournisseur){

    ui->lineEdit_nomFournisseur->setText ( nomFournisseur );
}









//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return )
        on_btn_valider_clicked ();

    else if( event->key () == Qt::Key_Escape )
        on_btn_quitter_clicked ();

}











//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::on_btn_valider_clicked()
{
    if( ui->lineEdit_nomFournisseur->text ().isEmpty () )
        emit on_message ("Veuillez saisir un nom de fournisseur", true);

    else{

        if( bdd.ajouterDonnee ( "tbl_fournisseur", "nomFournisseur", ui->lineEdit_nomFournisseur->text () ) ){

            emit on_message ("Votre fournisseur vient d'�tre ajout�", false);
            ui->comboBox_fournisseur->clear ();
            ui->lineEdit_nomFournisseur->clear ();
            listerFournisseur ();
        }
    }
}








//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::envoyerMessage (QString message, bool alerte){

    emit on_message ( message, alerte );
}







//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::on_btn_quitter_clicked(){

    close();
}








//----------------------------------------------------------------------------------------------------------------------
void Form_fournisseur::on_btn_modifier_clicked(){

    if( ui->lineEdit_nouveauFrs->text ().isEmpty () && ui->lineEdit_nomFournisseur->text ().isEmpty () )
        emit on_message ("Veuillez indiquer la nouvelle cat�gorie et/ou la cat�gorie actuelle", true);

    else{
        bdd.majDonnee ("tbl_fournisseur", "nomFournisseur", ui->lineEdit_nouveauFrs->text (),
                        "nomFournisseur", ui->lineEdit_nomFournisseur->text () );

        emit on_message ("Votre cat�gorie a �t� modifi�e", false);
        listerFournisseur ();
        ui->lineEdit_nomFournisseur->clear ();
        ui->lineEdit_nouveauFrs->clear ();

        close();
    }
}
