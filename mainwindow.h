#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#define DEVISE          QString("Euro(s)")

#include <QMainWindow>
#include <QString>
#include <QStringList>
#include <QDate>
#include <QDateTime>
#include <QMessageBox>
#include <QTableWidgetItem>
#include <QPrintDialog>
#include <QPrinter>
#include <QPainter>
#include <QKeyEvent>
#include <QPair>
#include <QList>

#include "database.h"
#include "form_enregisterproduits.h"
#include "form_rechercheproduits.h"
#include "form_ajoutcategorie.h"
#include "form_fournisseur.h"
#include "form_statsvente.h"
#include "form_client.h"
#include "form_rechercheclient.h"
#include "form_alerte.h"
#include "form_recherchevente.h"
#include "form_typeclient.h"
#include "form_recherchecategorie.h"
#include "form_stock.h"
#include "configuration.h"
#include "form_preference.h"
#include "facturepdf.h"
#include "form_recherchefournisseur.h"


#define NOM_FICHIER_LOG             QString(QCoreApplication::applicationDirPath () % "/erreur.log")
#define NOM_FICHIER_CONFIG          QString(QCoreApplication::applicationDirPath () % "/config.ini")


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void ecrireIHM(QString titre, QString msg);
//    void envoyerEmail();
    void debugger(QString titre, QString message);

    void ecrireLog(QString nomFichier, QString message, bool horodatage);

private slots:
    void on_actionEngistrement_produit_triggered();

    void afficherMessage(QString message, bool avertissement);

    void keyPressEvent ( QKeyEvent * event );

    void ajouterArticle(QStringList listeDonnees);

    void on_actionAjouter_cat_gorie_triggered();

    void on_actionAjouter_fournisseur_triggered();

    void on_actionStatistiques_ventes_triggered();

    void on_lineEdit_reduction_textChanged(const QString &arg1);

    void on_btn_finaliserCommande_clicked();

    void on_btn_retirerArticle_clicked();

    void on_checkBox_fidelisation_clicked(bool checked);

    void on_checkBox_clicked(bool checked);

    void on_actionAjouter_client_triggered();

    void receptionClient(QString listeDonneesClient);

    void on_actionQuitter_triggered();

    void receptionAlerteProduit(QString nomProduit, QString libelle, QString fournisseur, int quantiteProd, int quantiteAlerte);

    void on_actionAlerte_triggered();

    void on_actionRechercher_produit_triggered();

    void on_actionRechercher_vente_triggered();

    void on_actionAjouter_type_client_triggered();

    void on_actionRechercher_client_triggered();

    void on_actionModifier_Supprimer_client_triggered();

    void on_actionModifier_Supprimer_produit_triggered();

    void on_actionModifier_Supprimer_Cat_gorie_triggered();

    void on_actionG_n_rer_r_cap_Stock_triggered();

    void on_radioButton_aucuneReduc_clicked();

    void on_tableWidget_articles_cellClicked(int row, int column);

    void on_actionEnvoyer_trace_triggered();

    void on_actionPr_f_rences_triggered();

    void on_actionModifier_Supprimer_fournisseur_triggered();

private:
    Ui::MainWindow *ui;

    Database bdd;

    QStringList headerTableau;
    QStringList headerTableauArticles;

    QString idClient;

    float total;
    QList<QPair<QString, QString> > listeArticles;

    bool siArticleSelection;

};

#endif // MAINWINDOW_H
