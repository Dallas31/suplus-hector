#include "database.h"

#define UTILISATEUR_BDD             QString("hector")
#define MOT_DE_PASSE_BDD            QString("surplus")
#define DATABASE                    QString("QSQLITE")
#define HOSTNAME                    QString("localhost")
#define DATABASE_NAME               QString(QCoreApplication::applicationDirPath () % "/bdd_stock")

Database::Database(){


}



//----------------------------------------------------------------------------------------------------------------------
bool Database::ajouterProduit (QString ref, QString libelle, QString description, QString idCategorie,
                               QString quantiteProduit, QString pu, QString prixFrs, QString idFournisseur,
                               QString alerte, QString seuilAlerte, QString solde, QString tauxSolde){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return( false );
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "INSERT INTO tbl_stock (reference, libelleProduit, descriptionProduit, idCategorie, quantiteProduit,"
                " prixUnitaire, prixFournisseur, idFournisseur, alerte, quantiteAlerte, solder, tauxSolde) "
                "VALUES('" % ref % "','" % libelle % "', '" % description % "', " % idCategorie % ","
                % quantiteProduit % ", " % pu % ", " % prixFrs % ", " % idFournisseur % ", '" % alerte % "', " % seuilAlerte % ","
                " '" % solde % "', '" % tauxSolde % "');";


        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), false);
            return( false );
        }
        else
            return( true );
    }
    fermerBDD ();
}











//----------------------------------------------------------------------------------------------------------------------
bool Database::ajouterClient(QString nomClient, QString prenomClient, QString sexeClient, QString dateNaissance, QString adresseMail,
                             QString adresseClient, QString telephone, QString age, QString point, QString idTypeClient){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return( false );
    }
    else{

        QSqlQuery sqlQuery;

        QString requete = "INSERT INTO tbl_client (nomClient, prenomClient, ageClient, sexe, dateNaissance, adresseMail, telephone ,"
                " adresseClient, pointFidelite, idTypeClient) "
                "VALUES('" % nomClient % "','" % prenomClient % "', '" % age % "', '" % sexeClient % "', '" % dateNaissance % "', '" % adresseMail
                % "', '" % telephone % "', '" % adresseClient % "'," % point % ", '" % idTypeClient % "');";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);
            fermerBDD ();
            return( false );
        }
        else{

            fermerBDD ();
            return( true );
        }
    }
}






















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::rechercheClient(QString nomClient){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        QStringList resultat;

        QSqlQuery sqlQuery;
        QString requete = "SELECT idClient, nomClient, prenomClient, adresseClient FROM tbl_client WHERE nomClient LIKE '%" % nomClient % "%';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);
            fermerBDD ();
        }

        //Parcours des r�sultats
        while( sqlQuery.next () ){

            resultat.push_back ( sqlQuery.value (0).toString ()
                                 % ";" % sqlQuery.value (1).toString ()
                                 % ";" % sqlQuery.value (2).toString ()
                                 % ";" % sqlQuery.value (3).toString () );

        }

        fermerBDD ();
        return( resultat );

    }
}



















//----------------------------------------------------------------------------------------------------------------------
bool Database::ajouterDonnee(QString table, QString colonne, QString valeur){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return(false);
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "INSERT INTO " % table % " (" % colonne % ") VALUES('" % valeur % "');";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : " % sqlQuery.lastError ().text (),
                             true );
            fermerBDD ();
            return( false );
        }
        else{

            fermerBDD();
            return( true );
        }
    }
}

















//----------------------------------------------------------------------------------------------------------------------
bool Database::enregistrerVente(QString date, QString total, QString idClient, QString articles, QString mode){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return( false );
    }
    else{

        QSqlQuery sqlQuery;

        QString requete = "INSERT INTO tbl_vente (dateVente, totalVente, idClient, articles, modePaiement) "
                "VALUES('" % date % "','" % total % "', " % idClient % ", '" % articles % "', '" % mode % "');";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);
            fermerBDD ();
            return( false );
        }
        else{

            fermerBDD ();
            return( true );
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
QString Database::getValeur(QString table, QString colonne, QString condition, QString valeur){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return(false);
    }
    else{

        QString resultat;
        QSqlQuery sqlQuery;
        QString requete = "SELECT " % colonne % " FROM " % table % " WHERE " % condition % " = '" % valeur % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);
            fermerBDD ();
            return( "-1" );
        }
        //Parcours des r�sultats
        while( sqlQuery.next () ){

            resultat = sqlQuery.value (0).toString ();
        }

        fermerBDD ();
        return( resultat );
    }

    fermerBDD ();
    return("-1");
}


























//----------------------------------------------------------------------------------------------------------------------
QStringList Database::chercherProduit(QString ref, QString quantite, bool recherche){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
    else{

        QStringList listeResultat;

        //On v�rifie qu'il existe un produit pour cette r�f�rence
        if( getNombreProduit (ref) == 0)
            emit on_message ("Aucun r�sultat le produit n� " % ref, true);

        else{

            //On v�rifie que la quantit� du produit soit sup�rieur � 0
            if( getQuantiteProduit ( ref ) == 0 && !recherche)
                emit on_message ("La quantit� du produit n� " % ref % " est � 0", true);

            else if( getQuantiteProduit ( ref ) < quantite.toInt () )
                emit on_message ("La quantit� demand�e n'est pas en stock", true);

            else{

                QSqlQuery sqlQuery;
                QString requete;
                float prix = 0;

                requete = "SELECT idProduit, libelleProduit, descriptionProduit, prixUnitaire, tauxSolde FROM tbl_stock WHERE reference = '" % ref % "';";

                //Si mauvaise ex�cution
                if( !sqlQuery.exec ( requete ) )
                    emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                                     % sqlQuery.lastError ().text (), true);

                else{

                    //Parcours des r�sultats
                    while( sqlQuery.next () ){

                        //Gestion des soldes
                        prix = sqlQuery.value (3).toFloat ();
                        if( sqlQuery.value (4).toFloat () > 0 )
                            prix = prix - ( (prix * sqlQuery.value (4).toFloat ())/100 );


                        listeResultat.push_back ( sqlQuery.value (0).toString ()
                                                  % ";" % sqlQuery.value (1).toString ()
                                                  % ";" % sqlQuery.value (2).toString ()
                                                  % ";" % QString::number ( prix )
                                                  % ";" % quantite
                                                  % ";" % QString::number ( prix * quantite.toInt () ) ) ;
                    }

                    fermerBDD ();
                    return(listeResultat);
                }
            }
        }
        fermerBDD ();
        return(listeResultat);
    }
}



















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::listerValeur(QString table, QString colonne){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
    else{

        QStringList listeResultat;
        QSqlQuery sqlQuery;
        QString requete = "SELECT " % colonne % " FROM " % table % ";";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                listeResultat.push_back ( sqlQuery.value (0).toString () );
            }

            fermerBDD ();
            return( listeResultat );
        }

        fermerBDD ();
        return(listeResultat);
    }
}








//----------------------------------------------------------------------------------------------------------------------
bool Database::majQuantite(QString id, int valeur){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return(false);
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT quantiteProduit, alerte, quantiteAlerte, libelleProduit, descriptionProduit, idFournisseur FROM tbl_stock WHERE  idProduit = '" % id % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            int quantiteProduit = 0;
            QString alerte;
            QString produit;
            QString descProduit;
            QString idFournisseur;
            int quantiteMini = 0;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                quantiteProduit = sqlQuery.value (0).toInt ();
                alerte = sqlQuery.value (1).toString ();
                quantiteMini = sqlQuery.value (2).toInt ();
                produit = sqlQuery.value (3).toString ();
                descProduit = sqlQuery.value (4).toString ();
                idFournisseur = sqlQuery.value (5).toString ();
            }

            quantiteProduit = quantiteProduit - valeur;

            requete = "UPDATE tbl_stock SET quantiteProduit = " % QString::number ( quantiteProduit ) % " WHERE idProduit = " % id % ";";

            //Si mauvaise ex�cution
            if( !sqlQuery.exec ( requete ) ){

                emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                                 % sqlQuery.lastError ().text (), true);
                fermerBDD ();
                return(false);
            }
            else{

                if ( alerte == "oui" && quantiteProduit <= quantiteMini )
                    emit alerteProduit (produit, descProduit, getValeur ("tbl_fournisseur", "nomFournisseur", "idFournisseur", idFournisseur),
                                        quantiteProduit, quantiteMini);

                else if( quantiteProduit == 0 )
                    emit alerteProduit (produit, descProduit, getValeur ("tbl_fournisseur", "nomFournisseur", "idFournisseur", idFournisseur),
                                        quantiteProduit, quantiteMini);

                fermerBDD ();
                return(true);
            }
        }
    }

}














//----------------------------------------------------------------------------------------------------------------------
bool Database::crediterPoint(QString idClient, int valeur){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return(false);
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT pointFidelite FROM tbl_client WHERE idClient = '" % idClient % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            int point = 0;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                point = sqlQuery.value (0).toInt ();
            }

            point = point + valeur;

            requete = "UPDATE tbl_client SET pointFidelite = " % QString::number ( point ) % " WHERE idClient = " % idClient % ";";

            //Si mauvaise ex�cution
            if( !sqlQuery.exec ( requete ) ){

                emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                                 % sqlQuery.lastError ().text (), true);
                fermerBDD ();
                return(false);
            }

            else{

                fermerBDD ();
                return(true);
            }
        }
    }
}












//----------------------------------------------------------------------------------------------------------------------
QString Database::getIdProduit(QString nomProduit, QString description){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT idProduit FROM tbl_stock WHERE libelleProduit = '" % nomProduit % "' AND descriptionProduit = '" % description % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            QString idProduit;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                idProduit = sqlQuery.value (0).toString ();
            }
            return( idProduit );
        }
    }
}









//----------------------------------------------------------------------------------------------------------------------
bool Database::supprimerDonnee (QString table, QString colonne, QString valeur){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() ){

        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
        return(false);
    }
    else{

        QSqlQuery sqlQuery;
        QString requete = "DELETE FROM " % table % " WHERE " % colonne % " = '" % valeur % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) ){

            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);
            return(false);
        }
        else{

            fermerBDD ();
            return( true );
        }
    }
}

















//----------------------------------------------------------------------------------------------------------------------
void Database::creerTable(){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_stock ( "
                " idProduit INTEGER PRIMARY KEY AUTOINCREMENT,"
                " reference VARCHAR(255),"
                " libelleProduit VARCHAR(255),"
                " descriptionProduit VARCHAR(255),"
                " idCategorie INTEGER (255),"
                " quantiteProduit INTEGER(100),"
                " prixUnitaire INTERGER(255),"
                " prixFournisseur INTEGER (255),"
                " idFournisseur INTEGER(255),"
                " alerte VARCHAR(3),"
                " quantiteAlerte INTEGER(255),"
                " solder VARCHAR(3),"
                " tauxSolde INTEGER )");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_categorie ( "
                " idCategorie INTEGER PRIMARY KEY AUTOINCREMENT,"
                " libelleCategorie VARCHAR(255) )");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_fournisseur ( "
                " idFournisseur INTEGER PRIMARY KEY AUTOINCREMENT,"
                " nomFournisseur VARCHAR(255) )");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_client ( "
                " idClient INTEGER PRIMARY KEY AUTOINCREMENT,"
                " idTypeClient INTEGER, "
                " nomClient VARCHAR(255),"
                " prenomClient VARCHAR(255),"
                " sexe VARCHAR(1),"
                " ageClient VARCHAR(3),"
                " dateNaissance VARCHAR(10),"
                " adresseMail VARCHAR(255),"
                " telephone VARCHAR(10),"
                " adresseClient VARCHAR (255),"
                " pointFidelite INTEGER)");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_vente ( "
                " idVente INTEGER PRIMARY KEY AUTOINCREMENT,"
                " dateVente VARCHAR(255),"
                " modePaiement VARCHAR(20),"
                " totalVente VARCHAR(255),"
                " idClient VARCHAR (255),"
                " articles TEXT )");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_typeClient ( "
                " idTypeClient INTEGER PRIMARY KEY AUTOINCREMENT,"
                " libelleTypeClient VARCHAR(255) )");

        //Cr�ation de la table si elle n'existe pas
        db.exec("CREATE TABLE IF NOT EXISTS tbl_alerte ( "
                " idAlerte INTEGER PRIMARY KEY AUTOINCREMENT,"
                " libelleAlerte VARCHAR(255) )"); 

        fermerBDD ();
    }
}
















//----------------------------------------------------------------------------------------------------------------------
void Database::fermerBDD(){

        db.commit ();
        db.close ();
        db.removeDatabase ( DATABASE_NAME );
}
















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::listerVente(QString dateDebut, QString dateFin){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete;

        if( dateFin == "-1" )
             requete = "SELECT dateVente, totalVente, idClient, modePaiement, articles FROM tbl_vente WHERE dateVente = '" % dateDebut % "';";
        else
            requete = "SELECT dateVente, totalVente, idClient, modePaiement, articles FROM tbl_vente WHERE dateVente >= '" % dateDebut
                    % "' AND dateVente <= '" % dateFin % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            QStringList resultat;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                if( sqlQuery.value (2).toString () != "0"){

                    resultat.push_back (sqlQuery.value (0).toString () % ";" % sqlQuery.value (1).toString () % ";"
                                        % getValeur ("tbl_client", "nomClient", "idClient", sqlQuery.value (2).toString () )
                                        % ";" % sqlQuery.value (3).toString () % ";" % sqlQuery.value (4).toString () );
                }
                else{

                    resultat.push_back (sqlQuery.value (0).toString () % ";" % sqlQuery.value (1).toString () % ";"
                                        % "Inconnu" % ";" % sqlQuery.value (3).toString () % ";" % sqlQuery.value (4).toString () );
                }
            }
            return( resultat );
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
void Database::majDonnee(QString table, QString colonne, QString nouvelleValeur, QString condition, QString valeur){



    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "UPDATE " % table % " SET " % colonne % " = '" % nouvelleValeur % "' WHERE " % condition % " = '" % valeur % "';";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);


        fermerBDD ();

    }
}













//----------------------------------------------------------------------------------------------------------------------
int Database::compterEnregistrement(QString table, QString colonne, QString valeur){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );

    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT COUNT(*) FROM " % table % " WHERE " % colonne % " = " % valeur % ";";

        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true );
        else{

            int resultat = 0;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                resultat = sqlQuery.value (0).toInt ();
            }
            return( resultat );
         }

        return(-1);
    }
}






















//----------------------------------------------------------------------------------------------------------------------
int Database::getQuantiteProduit(QString id){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT quantiteProduit FROM tbl_stock WHERE reference = '" % id % "';";

        int nbResultat = 0;

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                nbResultat = sqlQuery.value (0).toInt ();
            }

            return( nbResultat );
        }
    }
}





















//----------------------------------------------------------------------------------------------------------------------
int Database::getNombreProduit(QString id){


    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT COUNT(*) FROM tbl_stock WHERE reference = '" % id % "';";

        int nbQuantite = 0;

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                nbQuantite = sqlQuery.value (0).toInt ();
            }
            return( nbQuantite );
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
QStringList Database::listerProduits(QString valeur){

    //Importation des param�tres de connexion
    db = QSqlDatabase::addDatabase( DATABASE );
    db.setHostName( HOSTNAME );
    db.setUserName( UTILISATEUR_BDD );
    db.setPassword( MOT_DE_PASSE_BDD );
    db.setDatabaseName( DATABASE_NAME );

    if( ! db.open() )
        emit on_message("Impossible se connecter � la base de donn�es - Veuillez v�rifier vos param�tres de connexion "
                        % db.lastError().text(), true );
    else{

        QSqlQuery sqlQuery;
        QString requete = "SELECT * FROM tbl_stock WHERE reference= '" % valeur % "';";

        if( valeur == "-1" )
            requete = "SELECT * FROM tbl_stock";

        //Si mauvaise ex�cution
        if( !sqlQuery.exec ( requete ) )
            emit on_message ("Impossible d'ex�cuter la requ�te [ " %  requete % " ] | ERREUR : "
                             % sqlQuery.lastError ().text (), true);

        else{

            QString detail;
            QStringList listeDetail;

            //Parcours des r�sultats
            while( sqlQuery.next () ){

                for( int i = 0 ; i < sqlQuery.record ().count () ; i++ ){

                    detail += sqlQuery.value (i).toString () % ";";
                }
                listeDetail.push_back ( detail );
                detail.clear ();
            }
            return( listeDetail );
        }
    }

}

