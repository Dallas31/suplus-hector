#include "form_enregisterproduits.h"
#include "ui_form_enregisterproduits.h"

Form_enregisterProduits::Form_enregisterProduits(QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_enregisterProduits)
{
    ui->setupUi(this);
    this->setWindowTitle ("Enregistrement nouveau produit");

    if( contexte == "ajout" )
        ui->btn_modifierProduit->setEnabled (false);
    else{

        ui->btn_modifierProduit->setEnabled ( true );
        ui->btn_enregistrerProduit->setEnabled ( false );
        ui->lineEdit_codeBarre->setReadOnly ( true );
    }

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    ui->comboBox_categories->addItems ( bdd.listerValeur ("tbl_categorie","libelleCategorie") );
    ui->comboBox_fournisseur->addItems ( bdd.listerValeur ( "tbl_fournisseur", "nomFournisseur") );

    on_checkBox_alerte_clicked( false );
    on_checkBox_solde_clicked ( false );

    ui->lineEdit_codeBarre->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nomProduit->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_prixFournisseur->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_PU->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_quantiteMini->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_quantiteProduit->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_tauxSolde->setAlignment ( Qt::AlignCenter );

    ui->lineEdit_codeBarre->setText ( QDateTime::currentDateTime ().toString ("yyyyMMddhhmmss") );
}

Form_enregisterProduits::~Form_enregisterProduits()
{
    delete ui;
}









//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::chargerProduit(QString reference, QString categorie, QString fournisseur, QString libelle,
                                             QString pu, QString prixFournisseur, QString quantite, QString desc, bool alerte,
                                             QString quantiteAlert, bool solde, QString tauxSolde){

    ui->btn_genererCodeBarre->setEnabled ( false );

    ui->lineEdit_codeBarre->setText ( reference );
    ui->comboBox_categories->setCurrentIndex ( ui->comboBox_categories->findText ( categorie ) );
    ui->comboBox_fournisseur->setCurrentIndex ( ui->comboBox_fournisseur->findText ( fournisseur ) );
    ui->lineEdit_nomProduit->setText ( libelle );
    ui->lineEdit_PU->setText ( pu );
    ui->lineEdit_prixFournisseur->setText ( prixFournisseur );
    ui->lineEdit_quantiteProduit->setText ( quantite );
    ui->textEdit->setText ( desc );

    if( alerte ){

        ui->checkBox_alerte->setChecked ( true );
        ui->label_qtiteMini->setVisible ( true );
        ui->lineEdit_quantiteMini->setVisible ( true );
        ui->lineEdit_quantiteMini->setText ( quantiteAlert );
    }

    if( solde ){

        ui->checkBox_solde->setChecked ( true );
        ui->label_solde->setVisible (true);
        ui->lineEdit_tauxSolde->setVisible ( true );
        ui->lineEdit_tauxSolde->setText ( tauxSolde );
    }

    QFile fichierCodeBarre;
    if( fichierCodeBarre.exists ( PATH_CODE_BARRE % "/" % reference % ".png" ) ){

        QPixmap pix( PATH_CODE_BARRE % "/" % reference % ".png" );
        ui->label_image->setPixmap ( pix );
        ui->label_image->setAlignment (Qt::AlignCenter);
    }
    else
        emit on_message ("Aucun code barre existe pour ce produit", true);
}













//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::viderIHM(){

    ui->lineEdit_codeBarre->clear ();
    ui->lineEdit_nomProduit->clear ();
    ui->lineEdit_prixFournisseur->clear ();
    ui->lineEdit_PU->clear ();
    ui->lineEdit_quantiteMini->clear ();
    ui->lineEdit_quantiteProduit->clear ();
    ui->checkBox_alerte->setChecked ( false );
    ui->checkBox_solde->setChecked ( false );
    ui->lineEdit_tauxSolde->clear ();
    ui->textEdit->clear ();

    ui->label_image->clear ();
}
















//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::on_btn_enregistrerProduit_clicked(){

        QString siAlerte = "non";
        QString quantiteMini = "0";

        QString siSolder = "non";
        QString tauxSolde = "0";

        QStringList listeValeur;
        listeValeur.push_back ( ui->lineEdit_codeBarre->text () );
        listeValeur.push_back ( ui->comboBox_categories->currentText () );
        listeValeur.push_back ( ui->comboBox_fournisseur->currentText () );
        listeValeur.push_back ( ui->lineEdit_nomProduit->text () );
        listeValeur.push_back ( ui->lineEdit_PU->text () );
        listeValeur.push_back ( ui->lineEdit_prixFournisseur->text () );
        listeValeur.push_back ( ui->lineEdit_quantiteProduit->text () );

        bool annuler = false;

        for( int i = 0 ; i < listeValeur.length () ; i++ ){

            if( listeValeur.at (i).isEmpty () ){

                annuler = true;
                emit on_message ("Veuillez saisir toutes les informations pour enregistrer votre produit", true);
                break;
            }
        }

        if( !annuler ){

        //On v�rifie si une alerte est mise sur ce produit
        if( ui->checkBox_alerte->isChecked () ){

            if( !ui->lineEdit_quantiteMini->text ().isEmpty () ){

                siAlerte = "oui";
                quantiteMini = ui->lineEdit_quantiteMini->text ();
            }
            else{

                emit on_message ("Veuillez indiquer une quantit� minimum", true);
                annuler = true;
            }

        }

        //On v�rifie ce produit est sold�
        if( ui->checkBox_solde->isChecked () ){

            if( !ui->lineEdit_tauxSolde->text ().isEmpty () ){

                siSolder = "oui";
                tauxSolde = ui->lineEdit_tauxSolde->text ();
            }
            else{

                emit on_message ("Veuillez indiquer une taux pour votre solde", true);
                annuler = true;
            }
        }

        if( !annuler ){

            //On ajoute les donn�es en base de donn�es
            if ( bdd.ajouterProduit ( ui->lineEdit_codeBarre->text (),
                                    ui->lineEdit_nomProduit->text (),
                                    ui->textEdit->toPlainText (),
                                    bdd.getValeur("tbl_categorie", "idCategorie","libelleCategorie", ui->comboBox_categories->currentText () ),
                                    ui->lineEdit_quantiteProduit->text (),
                                    ui->lineEdit_PU->text (),
                                    ui->lineEdit_prixFournisseur->text (),
                                    bdd.getValeur ("tbl_fournisseur", "idFournisseur", "nomFournisseur", ui->comboBox_fournisseur->currentText () ),
                                    siAlerte, quantiteMini, siSolder, tauxSolde ) ){

                emit on_message ("Produit ajout�", false);

                //R�-initialisation de l'IHM
                viderIHM();
            }
        }
    }
}











//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return ){

        if( ui->btn_enregistrerProduit->isEnabled () )
            on_btn_enregistrerProduit_clicked ();

        else if( ui->btn_modifierProduit->isEnabled () );
            on_btn_modifierProduit_clicked ();
    }

    else if( event->key () == Qt::Key_Escape ){

        QFile fichierCodeBarre(PATH_CODE_BARRE % "/" % ui->lineEdit_codeBarre->text () % ".png");
        if( fichierCodeBarre.exists () )
            fichierCodeBarre.remove ();

        close ();
    }


}











//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::envoyerMessage (QString message, bool alerte){

    emit on_message ( message, alerte );
}












//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::on_checkBox_alerte_clicked(bool checked){

    ui->label_qtiteMini->setVisible ( checked );
    ui->lineEdit_quantiteMini->setVisible ( checked );
}








//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::on_btn_modifierProduit_clicked(){

    QStringList listeValeur;
    listeValeur.push_back ( ui->lineEdit_codeBarre->text () );
    listeValeur.push_back ( ui->comboBox_categories->currentText () );
    listeValeur.push_back ( ui->comboBox_fournisseur->currentText () );
    listeValeur.push_back ( ui->lineEdit_nomProduit->text () );
    listeValeur.push_back ( ui->lineEdit_PU->text () );
    listeValeur.push_back ( ui->lineEdit_prixFournisseur->text () );
    listeValeur.push_back ( ui->lineEdit_quantiteProduit->text () );

    bool annuler = false;

    for( int i = 0 ; i < listeValeur.length () ; i++ ){

        if( listeValeur.at (i).isEmpty () ){

            annuler = true;
            emit on_message ("Veuillez saisir toutes les informations pour enregistrer votre produit", true);
            break;
        }
    }

    if( !annuler ){

        QString valeurAlerte = "non";
        QString quantiteAlerte = "0";

        QString valeurSolde = "non";
        QString tauxSolde = "0";

        if( ui->checkBox_alerte->isChecked () ){

            valeurAlerte = "oui";
            quantiteAlerte = ui->lineEdit_quantiteMini->text ();
        }

        if( ui->checkBox_solde->isChecked () ){

            valeurSolde = "oui";
            tauxSolde = ui->lineEdit_tauxSolde->text ();
        }


        bdd.majDonnee ("tbl_stock", "libelleProduit", ui->lineEdit_nomProduit->text (), "reference", ui->lineEdit_codeBarre->text () );
        bdd.majDonnee ("tbl_stock", "descriptionProduit", ui->textEdit->toPlainText (), "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "idCategorie", bdd.getValeur ("tbl_categorie","idCategorie", "libelleCategorie", ui->comboBox_categories->currentText () )
                       , "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "quantiteProduit", ui->lineEdit_quantiteProduit->text (), "reference", ui->lineEdit_codeBarre->text () );
        bdd.majDonnee ("tbl_stock", "prixUnitaire", ui->lineEdit_PU->text (), "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "prixFournisseur", ui->lineEdit_prixFournisseur->text (), "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "idFournisseur", bdd.getValeur ("tbl_fournisseur","idFournisseur", "nomFournisseur", ui->comboBox_fournisseur->currentText () )
                                                                    , "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "alerte", valeurAlerte, "reference", ui->lineEdit_codeBarre->text () );
        bdd.majDonnee ("tbl_stock", "quantiteAlerte", quantiteAlerte, "reference", ui->lineEdit_codeBarre->text () );

        bdd.majDonnee ("tbl_stock", "solder", valeurSolde, "reference", ui->lineEdit_codeBarre->text () );
        bdd.majDonnee ("tbl_stock", "tauxSolde", tauxSolde, "reference", ui->lineEdit_codeBarre->text () );

        emit on_message ("Produit mis � jour", false);
    }
}










//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::on_checkBox_solde_clicked(bool checked){

    ui->label_solde->setVisible ( checked );
    ui->lineEdit_tauxSolde->setText ("0");
    ui->lineEdit_tauxSolde->setVisible ( checked );
}













//----------------------------------------------------------------------------------------------------------------------
void Form_enregisterProduits::on_btn_genererCodeBarre_clicked(){

    if( !ui->lineEdit_codeBarre->text ().isEmpty () ){

        QFile fichierCodeBarre;
        if( !fichierCodeBarre.exists ( PATH_CODE_BARRE % "/" % ui->lineEdit_codeBarre->text () % ".png" ) ){

            if( bdd.compterEnregistrement ( "tbl_stock", "reference", ui->lineEdit_codeBarre->text () ) == 0 ){

                QProcess processus;
                QStringList listeArguments;
                listeArguments << "-o"
                               << PATH_CODE_BARRE % "/" % ui->lineEdit_codeBarre->text () % ".png"
                               << "-d"
                               << ui->lineEdit_codeBarre->text ();

                processus.start ( PATH_ZINT, listeArguments);
                processus.waitForFinished (-1);

                QPixmap pix( PATH_CODE_BARRE % "/" % ui->lineEdit_codeBarre->text () % ".png" );
                ui->label_image->setPixmap ( pix );
                ui->label_image->setAlignment (Qt::AlignCenter);
            }
            else
                emit on_message ("Un produit avec la r�f�rence " % ui->lineEdit_codeBarre->text () % " existe d�j� en base de donn�es", true);
        }
        else
            emit on_message ("Un code barre a d�j� �t� g�n�r� pour cette r�f�rence", true);
    }
    else
        emit on_message ("Veuillez saisir une r�f�rence pour g�n�rer un code barre ", true);
}
