#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <QSettings>

class Configuration
{
public:
    Configuration();

    QString getValue(QString fichierIni, QString groupe, QString cle);
    bool setValue(QString fichierIni, QString groupe, QString cle, QString valeur);
};

#endif // CONFIGURATION_H
