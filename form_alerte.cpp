#include "form_alerte.h"
#include "ui_form_alerte.h"

Form_alerte::Form_alerte(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_alerte)
{
    ui->setupUi(this);

    this->setWindowTitle ("Tableau des alertes");

    ui->btn_supprimerAlerte->setEnabled ( false );

    listerAlertes();
}

Form_alerte::~Form_alerte()
{
    delete ui;
}












//----------------------------------------------------------------------------------------------------------------------
void Form_alerte::listerAlertes(){

    QStringList listeAlerte =  bdd.listerValeur ("tbl_alerte", "libelleAlerte");

    for( int i = 0 ; i < listeAlerte.length () ; i++ ){

        ui->listWidget_alerte->insertItem (0, listeAlerte.at (i));
    }
}













//----------------------------------------------------------------------------------------------------------------------
void Form_alerte::on_btn_supprimerAlerte_clicked(){

    if ( ui->listWidget_alerte->count () == 0 )
        emit on_message ("Aucune alerte", true);
    else{

        if ( ui->listWidget_alerte->currentItem ()->text ().isEmpty () )
            emit on_message ("Aucune alerte sélectionnée", true);

        else{

            if( ui->checkBox_toutes->isChecked () ){

                for(int j = 0 ; j < ui->listWidget_alerte->count () ; j++){

                    bdd.supprimerDonnee ("tbl_alerte", "libelleAlerte", ui->listWidget_alerte->item (j)->text () );
                }
                ui->listWidget_alerte->clear ();
                emit on_message ("Alertes supprimées", false);
            }
            else if( bdd.supprimerDonnee ("tbl_alerte", "libelleAlerte", ui->listWidget_alerte->currentItem ()->text () ) ){

                emit on_message ("Alerte supprimée", false);
                delete ui->listWidget_alerte->currentItem ();
            }
        }
    }
}












//----------------------------------------------------------------------------------------------------------------------
void Form_alerte::on_listWidget_alerte_itemClicked(QListWidgetItem *item){

    ui->btn_supprimerAlerte->setEnabled ( true );
}
