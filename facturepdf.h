#ifndef FACTUREPDF_H
#define FACTUREPDF_H

#include <QPrinter>
#include <QPainter>
#include <QLineF>
#include <QDateTime>
#include <QStringBuilder>

class facturePDF
{
public:
    facturePDF();

    void creerFacture(QString pathFichier, QString texte);
};

#endif // FACTUREPDF_H
