#ifndef FORM_TYPECLIENT_H
#define FORM_TYPECLIENT_H

#include <QWidget>
#include <QMessageBox>
#include <QKeyEvent>

#include "database.h"

namespace Ui {
class Form_typeClient;
}

class Form_typeClient : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_typeClient(QWidget *parent = 0);
    ~Form_typeClient();

    void listerTypeClient();
    
private slots:
    void on_btn_enregistrer_clicked();

    void on_btn_quitter_clicked();

    void keyPressEvent(QKeyEvent *event);

    void envoyerMessage(QString message, bool alerte);

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_typeClient *ui;

    Database bdd;
};

#endif // FORM_TYPECLIENT_H
