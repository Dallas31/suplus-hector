#ifndef FORM_RECHERCHECATEGORIE_H
#define FORM_RECHERCHECATEGORIE_H

#include <QWidget>
#include <QMessageBox>
#include <QListWidgetItem>

#include "database.h"
#include "form_ajoutcategorie.h"

namespace Ui {
class Form_rechercheCategorie;
}

class Form_rechercheCategorie : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_rechercheCategorie(QWidget *parent = 0);
    ~Form_rechercheCategorie();

signals:
    void on_message(QString message, bool alerte);
    
private slots:
    void on_btn_supprimer_clicked();

    void on_btn_modifier_clicked();

    void envoyerMessage(QString message, bool alerte);

    void listerCategorie();

private:
    Ui::Form_rechercheCategorie *ui;

    Database bdd;
    QStringList headerTableau;
};

#endif // FORM_RECHERCHECATEGORIE_H
