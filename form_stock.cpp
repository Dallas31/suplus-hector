#include "form_stock.h"
#include "ui_form_stock.h"

Form_stock::Form_stock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_stock)
{
    ui->setupUi(this);
    this->setWindowTitle ("R�cap. stock");

    headerTableau << "Nom produit" << "Description produit" << "Fournisseur" << "Quantite produit";
    ui->tableWidget_stock->setColumnCount ( headerTableau.length () );
    ui->tableWidget_stock->setHorizontalHeaderLabels ( headerTableau );
    ui->tableWidget_stock->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch);

    ui->lineEdit_nomProduit->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_fournisseur->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_quantiteMini->setAlignment ( Qt::AlignCenter );

    afficherStock();
}

Form_stock::~Form_stock()
{
    delete ui;
}








//----------------------------------------------------------------------------------------------------------------------
void Form_stock::afficherStock(){

    Rapport *rapport = new Rapport();
    QStringList listeDonnee = rapport->genererRapport ();

    ui->tableWidget_stock->setRowCount ( listeDonnee.length () );

    for(int i = 0 ; i < listeDonnee.length () ; i++){

        QStringList listeDonneeDecoupee = listeDonnee.at (i).split (";");

        for(int j = 0 ; j < listeDonneeDecoupee.length () ; j++){

            QTableWidgetItem *item = new QTableWidgetItem( listeDonneeDecoupee.at (j) );
            item->setTextAlignment ( Qt::AlignCenter );
            ui->tableWidget_stock->setItem (i,j, item );
        }
    }
}












//----------------------------------------------------------------------------------------------------------------------
void Form_stock::afficherRecherche(int indexColonne, QString valeur){

    for (int i = 0 ; i < ui->tableWidget_stock->rowCount() ; i++) {

        if( ! ui->tableWidget_stock->item( i, indexColonne)->text().contains( valeur ) )
            ui->tableWidget_stock->setRowHidden( i ,true);
        else
            ui->tableWidget_stock->setRowHidden( i ,false);
    }
}








//----------------------------------------------------------------------------------------------------------------------
void Form_stock::on_lineEdit_nomProduit_textChanged(const QString &arg1){

    afficherRecherche (0, arg1);
}







//----------------------------------------------------------------------------------------------------------------------
void Form_stock::on_lineEdit_fournisseur_textChanged(const QString &arg1){

    afficherRecherche (2, arg1);
}









//----------------------------------------------------------------------------------------------------------------------
void Form_stock::on_lineEdit_quantiteMini_textChanged(const QString &arg1){

    if( arg1.isEmpty () ){

        ui->tableWidget_stock->setRowCount (0);
        afficherStock ();
    }
    else{

        for (int i = 0 ; i < ui->tableWidget_stock->rowCount() ; i++) {

            if( ui->tableWidget_stock->item( i, 3)->text().toInt () <= arg1.toInt () )
                ui->tableWidget_stock->setRowHidden( i ,false);
            else
                ui->tableWidget_stock->setRowHidden( i ,true);
        }
    }
}



