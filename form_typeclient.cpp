#include "form_typeclient.h"
#include "ui_form_typeclient.h"

Form_typeClient::Form_typeClient(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_typeClient)
{
    ui->setupUi(this);
    listerTypeClient ();

    connect (&bdd,SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    ui->lineEdit_typeClient->setAlignment ( Qt::AlignCenter );
}

Form_typeClient::~Form_typeClient()
{
    delete ui;
}

//----------------------------------------------------------------------------------------------------------------------
void Form_typeClient::listerTypeClient(){

    ui->comboBox_typeClient->clear ();
    ui->comboBox_typeClient->addItems ( bdd.listerValeur ("tbl_typeClient","libelleTypeClient") );
}








//----------------------------------------------------------------------------------------------------------------------
void Form_typeClient::on_btn_enregistrer_clicked(){

    if( !ui->lineEdit_typeClient->text ().isEmpty () ){

        bdd.ajouterDonnee ("tbl_typeClient", "libelleTypeClient", ui->lineEdit_typeClient->text () );
        listerTypeClient ();

        emit on_message ("Nouveau type de client ajout�", false);

        ui->lineEdit_typeClient->clear ();
    }
    else
        emit on_message ("Veuillez renseigner un nouveau type de client", true);
}









//----------------------------------------------------------------------------------------------------------------------
void Form_typeClient::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return )
        on_btn_enregistrer_clicked ();

    else if( event->key () == Qt::Key_Escape )
        on_btn_quitter_clicked ();

}










//----------------------------------------------------------------------------------------------------------------------
void Form_typeClient::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}











//----------------------------------------------------------------------------------------------------------------------
void Form_typeClient::on_btn_quitter_clicked(){

     close();
}
