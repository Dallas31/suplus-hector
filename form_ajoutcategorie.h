#ifndef FORM_AJOUTCATEGORIE_H
#define FORM_AJOUTCATEGORIE_H

#include <QWidget>
#include "database.h"
#include <QStringList>
#include <QString>
#include <QMessageBox>
#include <QKeyEvent>

namespace Ui {
class Form_ajoutCategorie;
}

class Form_ajoutCategorie : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_ajoutCategorie(QString contexte, QWidget *parent = 0);
    ~Form_ajoutCategorie();

    void listerCategorie();

    void chargerCategorie(QString categorie);

private slots:

    void envoyerMessage(QString message, bool alerte);
    
    void on_btn_valider_clicked();

    void on_btn_quitter_clicked();

    void keyPressEvent(QKeyEvent *event);

    void on_btn_modifier_clicked();

signals:

    void on_message(QString message, bool alerte);

    void categorieModifie();

private:
    Ui::Form_ajoutCategorie *ui;

    Database bdd;
};

#endif // FORM_AJOUTCATEGORIE_H
