#include "form_recherchevente.h"
#include "ui_form_recherchevente.h"

Form_rechercheVente::Form_rechercheVente(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_rechercheVente)
{
    ui->setupUi(this);
    this->setWindowTitle ("Rechercher une vente");
    clicID = 0;

    header << "Date vente" << "Total (Euro)" << "Client" << "Mode paiement" << "Article(s)";
    ui->tableWidget_vente->setColumnCount ( header.length () );
    ui->tableWidget_vente->setHorizontalHeaderLabels ( header );
    ui->tableWidget_vente->horizontalHeader ()->setResizeMode (QHeaderView::Stretch);

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    ui->lineEdit_client->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_dateDebut_vente->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_dateFin_vente->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_montant->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_CA->setAlignment ( Qt::AlignCenter );

}

Form_rechercheVente::~Form_rechercheVente()
{
    delete ui;
}










//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::afficherVente(QStringList listeVente){

    float chiffreAffaire = 0;

    ui->tableWidget_vente->clear ();
    ui->tableWidget_vente->setHorizontalHeaderLabels ( header );
    ui->tableWidget_vente->setRowCount ( listeVente.length () );

    for( int i = 0 ; i < listeVente.length () ; i++ ){

        QStringList itemDecoupee = listeVente.at (i).split (";");

        for( int j = 0 ; j < itemDecoupee.length () ; j++ ){

            QTableWidgetItem *item = new QTableWidgetItem( itemDecoupee.at (j) );
            item->setTextAlignment ( Qt::AlignCenter);
            ui->tableWidget_vente->setItem (i,j, item);

            if( j == header.indexOf ("Total (Euro)") )
                chiffreAffaire += itemDecoupee.at (j).toFloat ();
        }
    }
    ui->lineEdit_CA->setText ( QString::number ( chiffreAffaire ) );
}














//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::afficherRecherche(int indexColonne, QString valeur){

    for (int i = 0 ; i < ui->tableWidget_vente->rowCount() ; i++) {

        if( ! ui->tableWidget_vente->item( i, indexColonne)->text().contains( valeur ) )
            ui->tableWidget_vente->setRowHidden( i ,true);
        else
            ui->tableWidget_vente->setRowHidden( i ,false);
    }
}
















//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_checkBox_journee_clicked(bool checked){

    ui->lineEdit_dateDebut_vente->clear ();
    ui->checkBox_periode->setVisible ( !checked );
    ui->label_au->setVisible ( !checked );

    ui->lineEdit_dateFin_vente->clear ();
    ui->lineEdit_dateFin_vente->setVisible ( !checked);

    ui->lineEdit_dateDebut_vente->setText ( QDate::currentDate ().toString ( "dd/MM/yyyy" ) );
}

















//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_checkBox_periode_clicked(bool checked){

    ui->lineEdit_dateDebut_vente->clear ();
    ui->lineEdit_dateFin_vente->clear ();
    ui->checkBox_journee->setVisible ( !checked );
}















//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_calendrier_clicked(const QDate &date){

    if( ui->checkBox_journee->isChecked () || ui->checkBox_periode->isChecked () ){

        if( ui->checkBox_journee->isChecked () )
            ui->lineEdit_dateDebut_vente->setText ( date.toString ("dd/MM/yyyy") );

        else if( ui->checkBox_periode->isChecked () ){

            if( clicID == 0 ){

                ui->lineEdit_dateDebut_vente->setText ( date.toString ("dd/MM/yyyy") );
                clicID = 1;
            }
            else if( clicID == 1){

                ui->lineEdit_dateFin_vente->setText ( date.toString ("dd/MM/yyyy") );
                clicID = 0;
            }
        }
    }
    else
        emit on_message ("Veuillez s�lectionner une recherche par journ�e ou par p�riode", true);
}











//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_btn_rechercherVente_clicked(){

    QString dateDebut;

    if( ui->checkBox_journee->isChecked () ){

        dateDebut = QDate::fromString (ui->lineEdit_dateDebut_vente->text (), "dd/MM/yyyy").toString ("yyyyMMdd");
        afficherVente( bdd.listerVente ( dateDebut, "-1") );
    }
    else if ( ui->checkBox_periode->isChecked () ){

        dateDebut = QDate::fromString (ui->lineEdit_dateDebut_vente->text (), "dd/MM/yyyy").toString ("yyyyMMdd");
        QString dateFin = QDate::fromString (ui->lineEdit_dateFin_vente->text (), "dd/MM/yyyy").toString ("yyyyMMdd");
        afficherVente( bdd.listerVente ( dateDebut, dateFin) );
    }
}









//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_tableWidget_vente_cellClicked(int row, int column){

    QString article = ui->tableWidget_vente->item (row, header.indexOf ("Article(s)"))->text ().replace ("/","\n");
    ui->textEdit_detailVente->setText ( article );

}









//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_lineEdit_client_textChanged(const QString &arg1){

    afficherRecherche (2, arg1);
}









//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::on_lineEdit_montant_textChanged(const QString &arg1){

    afficherRecherche (1, arg1);
}









//---------------------------------------------------------------------------------------------------------------------
void Form_rechercheVente::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte);
}
