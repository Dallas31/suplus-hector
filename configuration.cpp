#include "configuration.h"

Configuration::Configuration()
{
}




//----------------------------------------------------------------------------------------------------------------------
QString Configuration::getValue(QString fichierIni, QString groupe, QString cle){

    QSettings settings ( fichierIni, QSettings::IniFormat );
    settings.beginGroup ( groupe );
    return(settings.value (cle, "-1").toString ());
}




//----------------------------------------------------------------------------------------------------------------------
bool Configuration::setValue(QString fichierIni, QString groupe, QString cle, QString valeur){

    QSettings settings ( fichierIni, QSettings::IniFormat );
    settings.beginGroup ( groupe );
    settings.setValue (cle, valeur);

    if( settings.value (cle).toString () == valeur )
        return(true);

    return(false);
}
