#include "facturepdf.h"

facturePDF::facturePDF()
{
}

void facturePDF::creerFacture(QString pathFichier, QString texte){

        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setFullPage (QPrinter::A4);
        printer.setOutputFileName(pathFichier);

        QPainter painter(&printer);
        painter.begin(&printer);
        painter.setFont(QFont("Verdana",12));
        painter.drawText(3200,500,texte);

        painter.end();
}
