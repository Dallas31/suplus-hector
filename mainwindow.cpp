#include "mainwindow.h"
#include "ui_mainwindow.h"




MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setWindowTitle ("Surplus Hector - Achat & Stock");
    this->setFixedSize (837,650);

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString, bool)));
    connect (&bdd,SIGNAL(alerteProduit(QString,QString,QString,int,int)), this, SLOT(receptionAlerteProduit(QString,QString,QString,int,int)));

    ui->lineEdit_quantite->setText ("1");

    total = 0;

    ui->lineEdit_reduction->setText ("0");
    ui->radioButton_aucuneReduc->setChecked ( true );
    ui->lineEdit_total->setReadOnly ( true );
    ui->lineEdit_nbArticle->setReadOnly ( true );

    ui->lineEdit_client->setVisible ( false );

    headerTableauArticles << "ID" << "Produit" << "Description" << "Prix unitaire" << "Quantit�" << "Total";
    ui->tableWidget_articles->setColumnCount ( headerTableauArticles.length () );
    ui->tableWidget_articles->horizontalHeader ()->setResizeMode (QHeaderView::Stretch);
    ui->tableWidget_articles->setHorizontalHeaderLabels ( headerTableauArticles);

    ui->btn_retirerArticle->setEnabled ( false );

    ui->lineEdit_codeBarre->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_client->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_nbArticle->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_quantite->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_reduction->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_total->setAlignment ( Qt::AlignCenter );

    ui->btn_finaliserCommande->setEnabled ( false );

    siArticleSelection = false;

    facturePDF *pdf = new facturePDF();
    pdf->creerFacture (QCoreApplication::applicationDirPath () % "/factureTest.pdf",
                       "Facture du " % QDateTime::currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss"));

    QStringList modePaiement;
    modePaiement << "CB" << "Esp�ce" << "Ch�que";
    ui->comboBox_modePaiement->addItems ( modePaiement );


    bdd.creerTable ();
}

MainWindow::~MainWindow()
{
    delete ui;
}









//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionEngistrement_produit_triggered(){

    Form_enregisterProduits *fenetreNouveauProduit = new Form_enregisterProduits("ajout");
    connect (fenetreNouveauProduit, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    fenetreNouveauProduit->show ();
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::afficherMessage(QString message, bool avertissement){

    if( avertissement ){

        ecrireLog ( NOM_FICHIER_LOG, message, true );
        QMessageBox::warning (NULL,"Message", message);
    }
    else
        QMessageBox::information (NULL,"Message", message);
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return )
        ajouterArticle ( bdd.chercherProduit ( ui->lineEdit_codeBarre->text(), ui->lineEdit_quantite->text (), false ) );

    else if ( event->key () == Qt::Key_F1 )
        on_actionAjouter_client_triggered ();

    else if ( event->key () == Qt::Key_F2 )
        on_actionEngistrement_produit_triggered ();

    else if ( event->key () == Qt::Key_F3 )
        on_actionRechercher_produit_triggered ();

    else if ( event->key () == Qt::Key_F4 )
        on_actionAjouter_fournisseur_triggered ();
}




















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::ajouterArticle(QStringList listeDonnees){

    if( !listeDonnees.isEmpty () ){

        if( listeDonnees.length () > 1 ){

            Form_rechecheProduits *produits = new Form_rechecheProduits( ui->lineEdit_codeBarre->text (),
                                                                         ui->lineEdit_quantite->text (), "recherche" );

            connect (produits, SIGNAL(produitValider(QStringList)), this, SLOT(ajouterArticle(QStringList)));
            connect (produits, SIGNAL( on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));

            produits->show ();
        }
        else{

            ui->btn_finaliserCommande->setEnabled ( true );

            QString idProduit;

            //D�sactiver le bouton permettant de retirer un article
            if( !ui->btn_retirerArticle->isEnabled () )
                ui->btn_retirerArticle->setEnabled ( true );

            for(int i = 0 ; i < listeDonnees.length () ; i++ ){      

                QStringList listeDonneeDecoupee = listeDonnees.at (i).split (";");

                ui->tableWidget_articles->insertRow (0);

                for(int j = 0 ; j < listeDonneeDecoupee.length () ; j++ ){

                    QTableWidgetItem *item = new QTableWidgetItem( listeDonneeDecoupee.at (j) );
                    item->setTextAlignment (Qt::AlignCenter);
                    ui->tableWidget_articles->setItem ( 0, j, item);

                    if( j == headerTableauArticles.indexOf ("ID") )
                        idProduit = listeDonneeDecoupee.at (j);

                    if( j == headerTableauArticles.indexOf ("Produit")
                            && ( bdd.getValeur ("tbl_stock", "solder", "idProduit", idProduit ) == "oui" ) )
                        item->setForeground ( Qt::red );

                    if( j == headerTableauArticles.indexOf ("Total") )
                        total += listeDonneeDecoupee.at (j).toFloat ();
                }
            }

            ui->lineEdit_total->setText (QString::number ( total ));

            ui->lineEdit_codeBarre->clear ();
            ui->lineEdit_quantite->setText ("1");
            ui->lineEdit_nbArticle->setText ( QString::number ( ui->tableWidget_articles->rowCount () ) );
        }
    }
}




















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::ecrireIHM(QString titre, QString msg){

    QMessageBox::warning (NULL, titre, msg);
}




























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::debugger(QString titre, QString message){

    QMessageBox::warning (NULL, titre, message);
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::ecrireLog(QString nomFichier, QString message, bool horodatage){

    QFile fichierLog( nomFichier );
    if( ! fichierLog.open ( QIODevice::Text | QIODevice::ReadWrite | QIODevice::Append ) )
        debugger ("Ouverture fichier log","Erreur ouverture fichier log - Erreur : " % fichierLog.errorString () );
    else{

        QTextStream fluxFlog(&fichierLog);

        if( horodatage )
            fluxFlog << QDateTime::currentDateTime ().toString ("dd/MM/yyyy hh:mm:ss") << " - " << message << endl;
        else
            fluxFlog << message << endl;

        fichierLog.close ();
    }
}



























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionAjouter_cat_gorie_triggered(){

    Form_ajoutCategorie *ajoutCategorie = new Form_ajoutCategorie("ajout");
    connect (ajoutCategorie, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    ajoutCategorie->show ();
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionAjouter_fournisseur_triggered(){

    Form_fournisseur *fournisseur = new Form_fournisseur("ajouter");
    connect (fournisseur, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    fournisseur->show ();
}
























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionStatistiques_ventes_triggered(){

    Form_statsVente *stats = new Form_statsVente();
    stats->show ();
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_lineEdit_reduction_textChanged(const QString &arg1){

    //Total sans r�duction sur le champs r�duction est vide
    if( arg1.isEmpty () )
        ui->lineEdit_total->setText ( QString::number ( total ) );

    float nouveau_total = 0;

    //Si la r�duction se fait en %
    if( ui->radioButton_pourcentage->isChecked () ){

        //Si % > 100
        if( arg1.toFloat () > 100 ){

            on_lineEdit_reduction_textChanged("0");
            ui->lineEdit_reduction->clear ();
            ecrireIHM ("Erreur montant r�duction","Veuillez faire attention � la r�duction que vous apportez");
        }
        else{

            nouveau_total = total - ( ( arg1.toFloat () * total ) / 100 );
            ui->lineEdit_total->setText ( QString::number ( nouveau_total ) );
        }
    }
    else if ( ui->radioButton_euros->isChecked () ){  //Si la r�duction se fait en �

        //Si l'utilisateur met un montant plus �lev� que le montant total
        if( arg1.toInt () > total ){

            on_lineEdit_reduction_textChanged("0");
            ui->lineEdit_reduction->clear ();
            ecrireIHM ("Erreur montant r�duction","Veuillez faire attention � la r�duction que vous apportez");
        }
        else{

            nouveau_total =  total - arg1.toFloat ();
            ui->lineEdit_total->setText ( QString::number ( nouveau_total ) );
        }
    }
}

























//----------------------------------------------------------------------------------------------------------------------
//TODO -> impression facture
void MainWindow::on_btn_finaliserCommande_clicked(){

    //On questionne l"utilisateur pour la confirmation de la commande
    int reponse = QMessageBox::question (NULL,"Confirmation vente","Souhaitez-vous confirmer la commande ?", QMessageBox::Yes | QMessageBox::No );

    //Positif
    if( reponse == QMessageBox::Yes ){

        QStringList listeDescriptionArticle;

        //Permet de lister les articles vendu
        for(int i = 0 ; i < ui->tableWidget_articles->rowCount () ; i++){

            QPair<QString, QString> paire;
            paire.first = ui->tableWidget_articles->item (i, headerTableauArticles.indexOf ("ID") )->text ();
            paire.second = ui->tableWidget_articles->item (i, headerTableauArticles.indexOf ("Quantit�") )->text ();

            listeDescriptionArticle.push_back ( paire.second % " x "
                                                % ui->tableWidget_articles->item (i,
                                                                                  headerTableauArticles.indexOf ("Produit") )->text () );
            listeArticles.push_back ( paire );
        }


        //Si le client est enregistr� en base de donn�es
        if( ui->checkBox_fidelisation->isChecked () ){

            if (!ui->lineEdit_client->text ().isEmpty () ){
                //On enregistre la vente avec la date, le total, l'idClient et la liste des articles vendu
                if( bdd.enregistrerVente ( QDate::currentDate ().toString ("yyyyMMdd"),
                                           ui->lineEdit_total->text (),
                                           idClient, listeDescriptionArticle.join ("/"),
                                           ui->comboBox_modePaiement->currentText () ) ){

                    //On cr�dite les points du client
                    if( bdd.crediterPoint ( idClient, ui->lineEdit_total->text ().toFloat () ) )
                        afficherMessage ("Vente enregistr�e & point fid�lit� cr�dit�", false);
                }
            }
            else
                afficherMessage ("Champs client vide", true);
        }
        else{

            //On enregistre la vente avec la date, le total, l'idClient (0) et la liste des articles vendu
            if( bdd.enregistrerVente ( QDate::currentDate ().toString ("yyyyMMdd"),
                                       ui->lineEdit_total->text (),
                                       "0", listeDescriptionArticle.join ("/"),
                                       ui->comboBox_modePaiement->currentText () ) )
                afficherMessage ("Vente enregistr�e", false);
        }

        //Permet de mettre � jour la quantit� des produits en stock par rapport � ce qui a �t� vendu
        for(int i = 0 ; i < listeArticles.length (); i++){

            //Maj quantit� en stock
            if( !bdd.majQuantite ( listeArticles.at (i).first, listeArticles.at (i).second.toInt () ) ){

                ecrireIHM ("Erreur mise � jour quantit�","Impossible de mettre � jour la quantit� du produit "
                           % bdd.getValeur ("tbl_stock","libelleProduit", "idProduit", listeArticles.at (i).first )
                           % " - " % bdd.getValeur ("tbl_stock","descriptionProduit", "idProduit", listeArticles.at (i).first ) );
            }
        }


        siArticleSelection = false;


        //R�initilisation de l'IHM
        total = 0;
        ui->lineEdit_codeBarre->clear ();
        ui->lineEdit_quantite->setText ("1");
        ui->lineEdit_nbArticle->setText ("0");
        ui->lineEdit_total->setText ("0");

        ui->tableWidget_articles->setRowCount (0);
        ui->btn_finaliserCommande->setEnabled ( false );
        ui->checkBox_fidelisation->setChecked ( false );
        ui->lineEdit_client->clear ();
    }
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_btn_retirerArticle_clicked(){

    if( !siArticleSelection )
        ecrireIHM ("S�lection produit","Veuillez s�lectionner un produit pour le retirer");
    else{

        int numeroLigne = ui->tableWidget_articles->currentItem ()->row ();

        total = total - ui->tableWidget_articles->item (numeroLigne, headerTableauArticles.indexOf ("Total") )->text ().toFloat ();

        ui->lineEdit_total->setText ( QString::number ( total ) );

        ui->tableWidget_articles->removeRow ( numeroLigne );

        ui->lineEdit_nbArticle->setText ( QString::number ( ui->tableWidget_articles->rowCount () ) );

        if( ui->tableWidget_articles->rowCount () == 0 ){

            ui->btn_finaliserCommande->setEnabled ( false );
            ui->btn_retirerArticle->setEnabled ( false );
        }
    }
}




























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_checkBox_fidelisation_clicked(bool checked){

    if( checked ){

        ui->lineEdit_client->setVisible ( true );

        Form_rechercheClient *rechercheClient = new Form_rechercheClient("recherche");
        connect(rechercheClient, SIGNAL(clientValider(QString)), this, SLOT(receptionClient(QString)));

        rechercheClient->show ();
    }
    else{

        ui->lineEdit_client->clear ();
        ui->lineEdit_client->setVisible ( false );
    }
}



























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::receptionClient(QString listeDonneesClient){

    if( listeDonneesClient != "-1"){

        ui->lineEdit_client->setText ( listeDonneesClient.split (";").at (1)
                                       % " "
                                       % listeDonneesClient.split (";").at (2) );

        idClient = listeDonneesClient.split (";").at (0);
    }
    else
        ui->checkBox_fidelisation->setChecked ( false );

}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_checkBox_clicked(bool checked){

    if( ui->tableWidget_articles->rowCount () == 0 )
        ecrireIHM ("Erreur r�duction","Veuillez faire une commande pour faire une r�duction");

    else{

        if( checked ){

            ui->radioButton_pourcentage->setChecked ( true );
            ui->lineEdit_reduction->setText ("10");
        }
        else{

            ui->radioButton_aucuneReduc->setChecked ( true );
            ui->lineEdit_reduction->setText ("0");
            ui->lineEdit_total->setText ( QString::number ( total ) );
        }
    }
}
















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionAjouter_client_triggered(){

    Form_client *client = new Form_client("ajout");
    connect (client, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    client->show ();
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionQuitter_triggered(){

    QCoreApplication::quit ();

}



















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::receptionAlerteProduit(QString nomProduit, QString libelle, QString fournisseur, int quantiteProd, int quantiteAlerte){

    QString message = "Le " % QDate::currentDate ().toString ("dd/MM/yyyy") % "- La quantit� du produit [ " % nomProduit
            % " - " % libelle % " - Fournisseur : " % fournisseur % " ] est au minimum "
       "- Quantit� alerte : " % QString::number ( quantiteAlerte ) % ""
       " - Quantit� actuelle : " % QString::number ( quantiteProd );

    bdd.ajouterDonnee ("tbl_alerte", "libelleAlerte", message );

//    envoyerEmail();
}























//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionAlerte_triggered(){

    Form_alerte *tableauAlerte = new Form_alerte();
    connect (tableauAlerte, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    tableauAlerte->show ();
}




















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionRechercher_produit_triggered(){

    Form_rechecheProduits *recherche = new Form_rechecheProduits("","0", "recherche");
    connect (recherche, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    recherche->show ();
}



















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionRechercher_vente_triggered(){

    Form_rechercheVente *vente = new Form_rechercheVente();
    connect (vente, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    vente->show ();

}





















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionAjouter_type_client_triggered(){

    Form_typeClient *typeClient = new Form_typeClient();
    connect (typeClient, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    typeClient->show ();
}
















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionRechercher_client_triggered(){

    Form_rechercheClient *rechercheClient = new Form_rechercheClient("recherche");
    rechercheClient->show ();
}











//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionModifier_Supprimer_client_triggered(){

    Form_rechercheClient *rechercheClient = new Form_rechercheClient("autre");
    connect (rechercheClient, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    rechercheClient->show ();
}













//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionModifier_Supprimer_produit_triggered(){

    Form_rechecheProduits *produit = new Form_rechecheProduits("","0", "autre");
    connect (produit, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    produit->show ();
}
















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionModifier_Supprimer_Cat_gorie_triggered(){

    Form_rechercheCategorie *categorie = new Form_rechercheCategorie();
    connect (categorie,SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    categorie->show ();
}















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionG_n_rer_r_cap_Stock_triggered(){

    Form_stock *stock = new Form_stock();
    stock->show ();
}
















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_radioButton_aucuneReduc_clicked(){

    ui->lineEdit_reduction->setText ("0");
    ui->checkBox->setChecked ( false );

}














//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_tableWidget_articles_cellClicked(int row, int column){

    siArticleSelection = true;
}


















//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionEnvoyer_trace_triggered(){

    Configuration *config = new Configuration();

    system( config->getValue ( NOM_FICHIER_CONFIG, "PATH","Postie" ).toLocal8Bit ()
            + " -host:" + (config->getValue ( NOM_FICHIER_CONFIG, "SMTP", "host" ) ).toLocal8Bit ()
            + " -to:" + (config->getValue ( NOM_FICHIER_CONFIG, "SMTP", "to" ) ).toLocal8Bit ()
            + " -from:" + (config->getValue ( NOM_FICHIER_CONFIG, "SMTP", "from" ) ).toLocal8Bit ()
            + " -s:\"Log de traitement - Erreur\" -nomsg -a:\"" + NOM_FICHIER_LOG.toLocal8Bit () + "\"") ;
}














//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionPr_f_rences_triggered(){

    Form_preference *preference = new Form_preference();
    preference->show ();
}






//----------------------------------------------------------------------------------------------------------------------
void MainWindow::on_actionModifier_Supprimer_fournisseur_triggered(){

    Form_rechercheFournisseur *frs = new Form_rechercheFournisseur();
    connect (frs, SIGNAL(on_message(QString,bool)), this, SLOT(afficherMessage(QString,bool)));
    frs->show ();

}
