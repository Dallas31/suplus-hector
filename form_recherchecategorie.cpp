#include "form_recherchecategorie.h"
#include "ui_form_recherchecategorie.h"

Form_rechercheCategorie::Form_rechercheCategorie(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_rechercheCategorie)
{
    ui->setupUi(this);
    this->setWindowTitle ("Modifier/Supprimer cat�gorie");

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    headerTableau << "ID" << "Cat�gorie";
    ui->tableWidget_categorie->setColumnCount ( headerTableau.length () );
    ui->tableWidget_categorie->setHorizontalHeaderLabels ( headerTableau );
    ui->tableWidget_categorie->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    listerCategorie();
}

Form_rechercheCategorie::~Form_rechercheCategorie()
{
    delete ui;
}











//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheCategorie::listerCategorie(){

    ui->tableWidget_categorie->setRowCount (0);

    QStringList listeCategorie = bdd.listerValeur ("tbl_categorie","idCategorie");

    for(int i = 0 ; i < listeCategorie.length () ; i++){

        ui->tableWidget_categorie->insertRow (i);

        QTableWidgetItem *itemId = new QTableWidgetItem( listeCategorie.at (i) );
        QTableWidgetItem *itemCategorie = new QTableWidgetItem( bdd.getValeur ( "tbl_categorie","libelleCategorie",
                                                                                "idCategorie",
                                                                                listeCategorie.at (i) ) );

        ui->tableWidget_categorie->setItem ( i, 0, itemId );
        itemId->setTextAlignment ( Qt::AlignCenter );
        ui->tableWidget_categorie->setItem ( i, 1, itemCategorie );
        itemCategorie->setTextAlignment ( Qt::AlignCenter);
    }
}













//----------------------------------------------------------------------------------------------------------------------
//TODO -> V�rifier si des produits existent sous cette cat�gorie
void Form_rechercheCategorie::on_btn_supprimer_clicked(){

    if( !ui->tableWidget_categorie->currentItem ()->text ().isEmpty () ){

        QString idCategorie = ui->tableWidget_categorie->item ( ui->tableWidget_categorie->currentRow (),
                                                                headerTableau.indexOf ("ID") )->text ();

        if( bdd.compterEnregistrement( "tbl_stock", "idCategorie", idCategorie ) == 0 ){

            int reponse = QMessageBox::question (NULL,"Confirmaer suppression",
                                                 "Voulez-vous supprimer cette cat�gorie de produit ?",
                                                 QMessageBox::Yes | QMessageBox::No);

            if( reponse == QMessageBox::Yes ){

                if( bdd.supprimerDonnee ( "tbl_categorie", "idCategorie",idCategorie ) ){

                    ui->tableWidget_categorie->removeRow ( ui->tableWidget_categorie->currentRow () );
                    emit on_message ("Cat�gorie supprim�e", false);
                }
                else
                    emit on_message ("Cat�gorie non supprim�e", true);
            }
        }
        else
            emit on_message ("Impossible de supprimer cette cat�gorie - Des produits existent sous cette cat�gorie",
                             true);
    }
    else
        emit on_message ("Veuillez s�lectionner une cat�gorie � supprimer", true);

}











//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheCategorie::on_btn_modifier_clicked(){

    Form_ajoutCategorie *categorie = new Form_ajoutCategorie("modifier");

    connect (categorie, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));
    connect (categorie, SIGNAL(categorieModifie()), this, SLOT(listerCategorie()));
    categorie->show ();
    categorie->chargerCategorie ( ui->tableWidget_categorie->item ( ui->tableWidget_categorie->currentRow (),
                                                                    headerTableau.indexOf ("Cat�gorie"))->text () );
}












//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheCategorie::envoyerMessage(QString message, bool alerte){

    emit on_message (message, alerte);
}
