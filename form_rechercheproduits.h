#ifndef FORM_RECHECHEPRODUITS_H
#define FORM_RECHECHEPRODUITS_H

#include <QObject>
#include <QWidget>
#include <QStringBuilder>
#include <QStringList>
#include <QString>
#include <QTableWidgetItem>
#include <QTableWidget>
#include <QMessageBox>
#include <windows.h>

#include "database.h"
#include "form_enregisterproduits.h"

namespace Ui {
class Form_rechecheProduits;
}

class Form_rechecheProduits : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_rechecheProduits(QString codeBarre, QString quantite, QString contexte, QWidget *parent = 0);
    ~Form_rechecheProduits();

    void chargerIHM(QString reference);

signals:

    void produitValider(QStringList produit);

    void on_message(QString erreur, bool alerte);
    
private slots:

    void on_lineEdit_codeBarre_returnPressed();

    void on_btn_supprimer_clicked();

    void on_btn_modifier_clicked();

    void on_tableWidget_produits_cellDoubleClicked(int row, int column);

    void envoyerMessage(QString message, bool alerte);

    void on_tableWidget_produits_itemClicked(QTableWidgetItem *item);

    void on_checkBox_afficherStock_clicked(bool checked);

private:
    Ui::Form_rechecheProduits *ui;

    Database bdd;

    QString quantiteProduit;
    QStringList headerTableau;
};

#endif // FORM_RECHECHEPRODUITS_H
