#-------------------------------------------------
#
# Project created by QtCreator 2016-05-19T16:12:36
#
#-------------------------------------------------

QT       += core gui sql

TARGET = lecteurCodeBarre
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    database.cpp \
    configuration.cpp \
    form_enregisterproduits.cpp \
    form_ajoutcategorie.cpp \
    form_fournisseur.cpp \
    form_statsvente.cpp \
    qcustomplot.cpp \
    form_client.cpp \
    form_rechercheclient.cpp \
    form_typeclient.cpp \
    form_alerte.cpp \
    form_recherchevente.cpp \
    form_recherchecategorie.cpp \
    form_rechercheproduits.cpp \
    rapport.cpp \
    form_stock.cpp \
    form_preference.cpp \
    facturepdf.cpp \
    form_recherchefournisseur.cpp

HEADERS  += mainwindow.h \
    database.h \
    configuration.h \
    form_enregisterproduits.h \
    form_ajoutcategorie.h \
    form_fournisseur.h \
    form_statsvente.h \
    qcustomplot.h \
    form_client.h \
    form_rechercheclient.h \
    form_typeclient.h \
    form_alerte.h \
    form_recherchevente.h \
    form_recherchecategorie.h \
    form_rechercheproduits.h \
    rapport.h \
    form_stock.h \
    form_preference.h \
    facturepdf.h \
    form_recherchefournisseur.h

FORMS    += mainwindow.ui \
    form_enregisterproduits.ui \
    form_ajoutcategorie.ui \
    form_fournisseur.ui \
    form_statsvente.ui \
    form_client.ui \
    form_rechercheclient.ui \
    form_typeclient.ui \
    form_alerte.ui \
    form_recherchevente.ui \
    form_recherchecategorie.ui \
    form_rechercheproduits.ui \
    form_stock.ui \
    form_preference.ui \
    form_recherchefournisseur.ui

RESOURCES +=
