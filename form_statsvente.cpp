#include "form_statsvente.h"
#include "ui_form_statsvente.h"

Form_statsVente::Form_statsVente(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_statsVente)
{
    ui->setupUi(this);
    this->setWindowTitle ("Statistiques ventes");

    clicID = 0;
}

Form_statsVente::~Form_statsVente()
{
    delete ui;
}



//----------------------------------------------------------------------------------------------------------------------
void Form_statsVente::on_calendrier_clicked(const QDate &date){

    if( ui->checkBox_journee->isChecked () || ui->checkBox_periode->isChecked () ){

        if( ui->checkBox_journee->isChecked () )
            ui->lineEdit_dateDebutStats->setText ( date.toString ("dd/MM/yyyy") );

        else if( ui->checkBox_periode->isChecked () ){

            if( clicID == 0 ){

                ui->lineEdit_dateDebutStats->setText ( date.toString ("dd/MM/yyyy") );
                clicID = 1;
            }
            else if( clicID == 1){

                ui->lineEdit_dateFinStats->setText ( date.toString ("dd/MM/yyyy") );
                clicID = 0;
            }
        }
    }
    else
        QMessageBox::warning (NULL,"S�lection crit�re","Veuillez s�lectionner une recherche par journ�e ou par p�riode");

}






//----------------------------------------------------------------------------------------------------------------------
void Form_statsVente::on_checkBox_journee_clicked(bool checked){

    ui->lineEdit_dateDebutStats->clear ();
    ui->checkBox_periode->setVisible ( !checked );
    ui->label_au->setVisible ( !checked );

    ui->lineEdit_dateFinStats->clear ();
    ui->lineEdit_dateFinStats->setVisible ( !checked);

}








//----------------------------------------------------------------------------------------------------------------------
void Form_statsVente::on_checkBox_periode_clicked(bool checked){

    ui->lineEdit_dateDebutStats->clear ();
    ui->lineEdit_dateFinStats->clear ();
    ui->checkBox_journee->setVisible ( !checked );

}







//----------------------------------------------------------------------------------------------------------------------
void Form_statsVente::on_btn_generer_clicked(){

    Database *bdd = new Database();

}







//----------------------------------------------------------------------------------------------------------------------
void Form_statsVente::on_btn_exporter_clicked(){

}








//----------------------------------------------------------------------------------------------------------------------
/*
  Permet d'afficher les donn�es sur le graphique
  */
void Form_statsVente::afficherGraphPoint(QVector<double> vecteurY, QVector<double> vecteurX){

    int lastValue = vecteurY.at ( vecteurY.size () - 1 );
    int maxValue = lastValue;

    for(int j = 0 ; j < vecteurY.size () ; j++){

        if( maxValue < vecteurY.at (j) )
            maxValue = vecteurY.at (j);
    }


    // create graph and assign data to it:
    ui->widget_stats->addGraph();
    ui->widget_stats->graph(0)->setData(vecteurX, vecteurY);

    //Legend
    ui->widget_stats->xAxis->setLabel("Montant");
    ui->widget_stats->yAxis->setLabel("Temps");

    // set axes ranges, so we see all data:
    ui->widget_stats->xAxis->setRange(0, vecteurY.size ());
    ui->widget_stats->yAxis->setRange(0, maxValue + 5);
    ui->widget_stats->replot();
}







