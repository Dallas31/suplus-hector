#ifndef FORM_RECHERCHECLIENT_H
#define FORM_RECHERCHECLIENT_H

#include <QWidget>
#include <QMessageBox>
#include <QListWidgetItem>

#include "database.h"
#include "form_client.h"

namespace Ui {
class Form_rechercheClient;
}

class Form_rechercheClient : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_rechercheClient(QString contexte, QWidget *parent = 0);
    ~Form_rechercheClient();

    void afficherIHM(QString mot);


    
private slots:
    void on_lineEdi_rchNomClient_textChanged(const QString &arg1);

    void on_listWidget_listeClient_itemDoubleClicked(QListWidgetItem *item);

    void on_btn_supprimer_clicked();

    void on_listWidget_listeClient_itemClicked(QListWidgetItem *item);

    void on_btn_modifier_clicked();

    void envoyerMessage(QString message, bool alerte);

    void on_btn_quitter_clicked();


signals:
    void clientValider(QString client);

    void on_message(QString message, bool alerte);


private:
    Ui::Form_rechercheClient *ui;

    Database bdd;
    QString contexteAppel;
};

#endif // FORM_RECHERCHECLIENT_H
