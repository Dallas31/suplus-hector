#ifndef FORM_RECHERCHEFOURNISSEUR_H
#define FORM_RECHERCHEFOURNISSEUR_H

#include <QWidget>
#include <QMessageBox>
#include <QListWidgetItem>

#include "database.h"
#include "form_fournisseur.h"



namespace Ui {
class Form_rechercheFournisseur;
}

class Form_rechercheFournisseur : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_rechercheFournisseur(QWidget *parent = 0);
    ~Form_rechercheFournisseur();

    void listerFournisseur();


signals:
    void on_message(QString message, bool alerte);

private slots:

    void envoyerMessage(QString message, bool alerte);
    
    void on_btn_supprimer_clicked();

    void on_btn_modifier_clicked();

private:
    Ui::Form_rechercheFournisseur *ui;

    Database bdd;
    QStringList headerTableau;
};

#endif // FORM_RECHERCHEFOURNISSEUR_H
