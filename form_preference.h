#ifndef FORM_PREFERENCE_H
#define FORM_PREFERENCE_H

#include <QWidget>
#include <QStringBuilder>
#include <QFileDialog>
#include <QCoreApplication>

#include "form_preference.h"
#include "configuration.h"


#define NOM_FICHIER_CONFIG          QString(QCoreApplication::applicationDirPath () % "/config.ini")

namespace Ui {
class Form_preference;
}

class Form_preference : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_preference(QWidget *parent = 0);
    ~Form_preference();

    void chargerDonnees();
    
private slots:
    void on_btn_parcourir_clicked();

private:
    Ui::Form_preference *ui;
};

#endif // FORM_PREFERENCE_H


