#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QString>
#include <QDate>
#include <QMessageBox>
#include <QStringBuilder>
#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QPair>
#include <QList>
#include <QSettings>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlResult>
#include <QDate>
#include <QCoreApplication>



class Database : public QObject
{
    Q_OBJECT

public:
    Database();

    bool ajouterProduit(QString ref, QString libelle, QString description, QString idCategorie,
                        QString quantiteProduit, QString pu, QString prixFrs, QString idFournisseur,
                        QString alerte, QString seuilAlerte, QString solde, QString tauxSolde);

    bool ajouterClient(QString nomClient, QString prenomClient, QString sexeClient, QString dateNaissance,
                       QString adresseMail, QString adresseClient, QString telephone, QString age, QString point, QString idTypeClient);

    QStringList rechercheClient(QString nomClient);
    bool ajouterDonnee(QString table, QString colonne, QString valeur);
    bool enregistrerVente(QString date, QString total, QString idClient, QString articles, QString mode);

    QString getValeur(QString table, QString colonne, QString condition, QString valeur);
    QStringList chercherProduit(QString ref, QString quantite, bool recherche);
    QStringList listerValeur(QString table, QString colonne);

    bool majQuantite(QString id, int valeur);
    bool crediterPoint(QString idClient, int valeur);
    QString getIdProduit(QString nomProduit, QString description);

    bool supprimerDonnee(QString table, QString colonne, QString valeur);
    void creerTable();
    void fermerBDD();

    QStringList listerVente(QString dateDebut, QString dateFin);
    void majDonnee(QString table, QString colonne, QString nouvelleValeur, QString condition, QString valeur);
    int compterEnregistrement(QString table, QString colonne, QString valeur);

    int getQuantiteProduit(QString id);
    int getNombreProduit(QString id);
    QStringList listerProduits(QString valeur);

signals:

    void on_message(QString message, bool alerte);

    void alerteProduit(QString produit, QString descProduit, QString fournisseur, int quantiteProd, int quantiteAlerte);

private:

    QSqlDatabase db;

};

#endif // DATABASE_H
