#ifndef FORM_ALERTE_H
#define FORM_ALERTE_H

#include <QWidget>
#include <QListWidgetItem>
#include <QStringList>

#include "database.h"


namespace Ui {
class Form_alerte;
}

class Form_alerte : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_alerte(QWidget *parent = 0);
    ~Form_alerte();

    void listerAlertes();
    
private slots:
    void on_btn_supprimerAlerte_clicked();

    void on_listWidget_alerte_itemClicked(QListWidgetItem *item);

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_alerte *ui;

    Database bdd;
};

#endif // FORM_ALERTE_H
