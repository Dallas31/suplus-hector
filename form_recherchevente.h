#ifndef FORM_RECHERCHEVENTE_H
#define FORM_RECHERCHEVENTE_H

#include <QWidget>
#include <QMessageBox>
#include <QDate>
#include <QTableWidgetItem>


#include "database.h"

namespace Ui {
class Form_rechercheVente;
}

class Form_rechercheVente : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_rechercheVente(QWidget *parent = 0);
    ~Form_rechercheVente();

    void afficherVente(QStringList listeVente);
    void afficherRecherche(int indexColonne, QString valeur);
    
private slots:

    void on_checkBox_journee_clicked(bool checked);

    void on_checkBox_periode_clicked(bool checked);

    void on_calendrier_clicked(const QDate &date);

    void on_btn_rechercherVente_clicked();

    void on_tableWidget_vente_cellClicked(int row, int column);

    void on_lineEdit_client_textChanged(const QString &arg1);

    void on_lineEdit_montant_textChanged(const QString &arg1);

    void envoyerMessage(QString message, bool alerte);

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_rechercheVente *ui;

    int clicID;
    QStringList header;

    Database bdd;
};

#endif // FORM_RECHERCHEVENTE_H
