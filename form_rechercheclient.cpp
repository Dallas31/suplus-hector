#include "form_rechercheclient.h"
#include "ui_form_rechercheclient.h"

Form_rechercheClient::Form_rechercheClient(QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_rechercheClient)
{
    ui->setupUi(this);

    this->setWindowTitle ("Recherche client");

    connect (&bdd,SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    contexteAppel = contexte;
    if( contexte != "recherche" ){

        ui->btn_modifier->setEnabled ( true );
        ui->btn_supprimer->setEnabled ( true );
        ui->btn_quitter->setEnabled( false );
    }
    else{

        ui->btn_quitter->setEnabled( true );
        ui->btn_modifier->setEnabled ( false );
        ui->btn_supprimer->setEnabled ( false );
    }

    afficherIHM("");


}

Form_rechercheClient::~Form_rechercheClient(){

    delete ui;
}








//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::afficherIHM(QString mot){

    ui->listWidget_listeClient->clear ();
    QStringList listeDonnees = bdd.rechercheClient ( mot );

    if( ! listeDonnees.isEmpty () ){

        for(int j = 0 ; j < listeDonnees.length () ; j++ ){

            QListWidgetItem *item = new QListWidgetItem( listeDonnees[j].replace (";", " - ") );
            ui->listWidget_listeClient->addItem ( item );
        }
    }
    else{

        QListWidgetItem *item = new QListWidgetItem("Aucun r�sultat");
        ui->listWidget_listeClient->addItem ( item );
    }
}






//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_lineEdi_rchNomClient_textChanged(const QString &arg1){

    ui->listWidget_listeClient->clear ();

    afficherIHM ( arg1 );

}










//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_listWidget_listeClient_itemDoubleClicked(QListWidgetItem *item){

    if( contexteAppel == "recherche" ){

        int resultat = QMessageBox::question (NULL,"Confirmation","Souhaitez-vous s�lectionner ce client ?", QMessageBox::Yes | QMessageBox::No );

        if( resultat == QMessageBox::Yes ){

            emit clientValider ( item->text ().replace (" - ",";") );
            close();
        }
    }
}







//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_btn_supprimer_clicked(){

    int resultat = QMessageBox::question (NULL,"Confirmation","Souhaitez-vous supprimer ce client ?", QMessageBox::Yes | QMessageBox::No );

    if( resultat == QMessageBox::Yes ){

        QString client = ui->listWidget_listeClient->currentItem ()->text ();

        if( bdd.supprimerDonnee ("tbl_client", "idClient", client.split (" ").at (0) ) ){

            emit on_message ("Client supprim�", false);
            delete ui->listWidget_listeClient->currentItem ();
        }
    }
}






//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::envoyerMessage (QString message, bool alerte){

    emit on_message ( message, alerte );
}








//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_listWidget_listeClient_itemClicked(QListWidgetItem *item){

    ui->label_point->setText ( "Point fid�lit� : "
                               % bdd.getValeur ("tbl_client","pointFidelite","idClient",
                                                ui->listWidget_listeClient->currentItem ()->text ().split (" ").at (0) ) );

}





//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_btn_modifier_clicked(){

    Form_client *client = new Form_client("modifier");
    client->show ();

    QString idClient = ui->listWidget_listeClient->currentItem ()->text ().split (" - ").at (0);
    QString typeClient = bdd.getValeur ("tbl_typeClient", "libelleTypeClient", "idTypeClient",
                                        bdd.getValeur ("tbl_client", "idTypeClient", "idClient", idClient) );

    QString nom = bdd.getValeur ("tbl_client","nomClient","idClient", idClient);
    QString prenom = bdd.getValeur ("tbl_client","prenomClient", "idClient", idClient);
    QString sexe = bdd.getValeur ("tbl_client","sexe", "idClient", idClient);
    QString age = bdd.getValeur ("tbl_client","ageClient", "idClient", idClient);
    QString adresse = bdd.getValeur ("tbl_client", "adresseClient", "idClient", idClient);
    QString dateNaissance = bdd.getValeur ("tbl_client", "dateNaissance", "idClient", idClient);
    QString email = bdd.getValeur ("tbl_client", "adresseMail", "idClient", idClient);
    QString tel = bdd.getValeur ("tbl_client", "telephone", "idClient", idClient);


    client->chargerClient ( idClient, typeClient, nom, prenom, sexe, age, adresse, dateNaissance, email, tel );
}








//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheClient::on_btn_quitter_clicked(){

    emit clientValider ("-1");
    close();
}
