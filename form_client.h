#ifndef FORM_CLIENT_H
#define FORM_CLIENT_H

#include <QWidget>
#include <QMessageBox>
#include <QKeyEvent>

#include "database.h"

namespace Ui {
class Form_client;
}

class Form_client : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_client(QString contexte, QWidget *parent = 0);
    ~Form_client();

    void viderIHM();
    void chargerClient(QString idClientSelect, QString typeClient, QString nomClient, QString prenomClient, QString sexe,
                       QString age, QString adresse, QString dateNaissance, QString email, QString tel);
    
private slots:
    void on_btn_quitter_clicked();

    void on_btn_enregistrerClient_clicked();

    void keyPressEvent(QKeyEvent *event);

    void envoyerMessage(QString message, bool alerte);

    void on_btn_modifierClient_clicked();

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_client *ui;

    Database bdd;
    QString sexeClient;
    QString idClient;
};

#endif // FORM_CLIENT_H
