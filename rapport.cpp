#include "rapport.h"

Rapport::Rapport(){


}



//----------------------------------------------------------------------------------------------------------------------
QStringList Rapport::genererRapport(){

    QStringList listeIdProduit = bdd.listerValeur ("tbl_stock"," idProduit");

    QString nomProduit;
    QString description;
    QString fournisseur;
    QString quantite;    

    QStringList listeRapporProduit;

    for(int i = 0 ; i < listeIdProduit.length () ; i++){

        nomProduit = bdd.getValeur ("tbl_stock", "libelleProduit", "idProduit", listeIdProduit.at (i) );
        description = bdd.getValeur ("tbl_stock", "descriptionProduit", "idProduit", listeIdProduit.at (i) );
        fournisseur = bdd.getValeur ("tbl_fournisseur", "nomFournisseur", "idFournisseur",
                                     bdd.getValeur ("tbl_stock", "idFournisseur", "idProduit", listeIdProduit.at (i)));
        quantite = bdd.getValeur ("tbl_stock", "quantiteProduit", "idProduit", listeIdProduit.at (i) );

        listeRapporProduit.push_back ( nomProduit % ";" % description % ";" % fournisseur % ";" % quantite);
    }
    return( listeRapporProduit );
}
