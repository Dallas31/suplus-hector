#include "form_ajoutcategorie.h"
#include "ui_form_ajoutcategorie.h"

Form_ajoutCategorie::Form_ajoutCategorie(QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_ajoutCategorie)
{
    ui->setupUi(this);

    this->setWindowTitle ("Ajout cat�gorie produit");

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    ui->lineEdit_categorie->setAlignment ( Qt::AlignCenter );

    listerCategorie();

    if( contexte == "ajout" ){

        ui->btn_modifier->setEnabled ( false );
        ui->label_nouvelleCategorie->setEnabled ( false );
        ui->lineEdit_nouvelleCategorie->setEnabled ( false );
    }
    else if( contexte == "modifier"){

        ui->btn_modifier->setEnabled ( true );
        ui->btn_valider->setEnabled ( false );

        ui->label->setEnabled ( false );
        ui->comboBox_categorie->setEnabled ( false );

        ui->lineEdit_nouvelleCategorie->setAlignment ( Qt::AlignCenter );
    }
}

Form_ajoutCategorie::~Form_ajoutCategorie()
{
    delete ui;
}







//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return )
        on_btn_valider_clicked ();

    else if( event->key () == Qt::Key_Escape )
        close ();

}









//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::listerCategorie(){

    ui->comboBox_categorie->clear ();
    ui->comboBox_categorie->addItems ( bdd.listerValeur ("tbl_categorie","libelleCategorie") );
}











//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::chargerCategorie(QString categorie){

    ui->lineEdit_categorie->setText ( categorie );
}















//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::envoyerMessage(QString message, bool alerte){

    emit on_message ( message, alerte );
}













//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::on_btn_valider_clicked(){

    if( ui->lineEdit_categorie->text ().isEmpty () )
        emit on_message ("Veuillez indiquer la nouvelle cat�gorie !", true);

    else{

        if( !ui->lineEdit_categorie->text ().contains (" - ") ){

            if ( bdd.ajouterDonnee ("tbl_categorie","libelleCategorie", ui->lineEdit_categorie->text () ) ){

                emit on_message ("Votre cat�gorie a �t� ajout�e", false);
                listerCategorie ();
                ui->lineEdit_categorie->clear ();
            }
        }
        else
            emit on_message ("Chaine de caract�re interdit : \" - \"", true);
    }
}










//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::on_btn_quitter_clicked(){

    close();
}







//----------------------------------------------------------------------------------------------------------------------
void Form_ajoutCategorie::on_btn_modifier_clicked(){

    if( ui->lineEdit_nouvelleCategorie->text ().isEmpty () && ui->lineEdit_categorie->text ().isEmpty () )
        emit on_message ("Veuillez indiquer la nouvelle cat�gorie et/ou la cat�gorie actuelle", true);

    else{

        bdd.majDonnee ("tbl_categorie", "libelleCategorie", ui->lineEdit_nouvelleCategorie->text (),
                        "libelleCategorie", ui->lineEdit_categorie->text () );

        emit on_message ("Votre cat�gorie a �t� modifi�e", false);
        listerCategorie ();
        ui->lineEdit_categorie->clear ();
        ui->lineEdit_nouvelleCategorie->clear ();

        close();
    }
}
