#ifndef FORM_FOURNISSEUR_H
#define FORM_FOURNISSEUR_H

#include <QWidget>
#include "database.h"
#include <QMessageBox>
#include <QKeyEvent>

namespace Ui {
class Form_fournisseur;
}

class Form_fournisseur : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_fournisseur(QString contexte, QWidget *parent = 0);
    ~Form_fournisseur();

    void listerFournisseur();
    void chargerFournisseur(QString nomFournisseur);
    
private slots:
    void on_btn_valider_clicked();

    void envoyerMessage(QString message, bool alerte);

    void on_btn_quitter_clicked();

    void keyPressEvent(QKeyEvent *event);

    void on_btn_modifier_clicked();

signals:

    void on_message(QString message, bool alerte);

    void fournisseurModifie();

private:
    Ui::Form_fournisseur *ui;

    Database bdd;
};

#endif // FORM_FOURNISSEUR_H
