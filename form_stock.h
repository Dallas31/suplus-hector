#ifndef FORM_STOCK_H
#define FORM_STOCK_H

#include <QWidget>
#include <QTableWidgetItem>
#include <QStringList>
#include <QMessageBox>

#include "database.h"
#include "rapport.h"


namespace Ui {
class Form_stock;
}

class Form_stock : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_stock(QWidget *parent = 0);
    ~Form_stock();

    void afficherStock();
    void afficherRecherche(int indexColonne, QString valeur);
    
private slots:
    void on_lineEdit_nomProduit_textChanged(const QString &arg1);

    void on_lineEdit_fournisseur_textChanged(const QString &arg1);

    void on_lineEdit_quantiteMini_textChanged(const QString &arg1);

private:
    Ui::Form_stock *ui;

    QStringList headerTableau;

    Database bdd;
};

#endif // FORM_STOCK_H
