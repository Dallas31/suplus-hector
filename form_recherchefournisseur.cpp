#include "form_recherchefournisseur.h"
#include "ui_form_recherchefournisseur.h"

Form_rechercheFournisseur::Form_rechercheFournisseur(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_rechercheFournisseur)
{
    ui->setupUi(this);

    this->setWindowTitle ("Modifier/Supprimer cat�gorie");

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    headerTableau << "ID" << "Fournisseur";
    ui->tableWidget_fournisseur->setColumnCount ( headerTableau.length () );
    ui->tableWidget_fournisseur->setHorizontalHeaderLabels ( headerTableau );
    ui->tableWidget_fournisseur->horizontalHeader ()->setResizeMode ( QHeaderView::Stretch );

    listerFournisseur();
}

Form_rechercheFournisseur::~Form_rechercheFournisseur()
{
    delete ui;
}









//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheFournisseur::listerFournisseur(){

    ui->tableWidget_fournisseur->setRowCount (0);

    QStringList listeFrs = bdd.listerValeur ("tbl_fournisseur","idFournisseur");

    for(int i = 0 ; i < listeFrs.length () ; i++){

        ui->tableWidget_fournisseur->insertRow (i);

        QTableWidgetItem *itemId = new QTableWidgetItem( listeFrs.at (i) );
        QTableWidgetItem *itemCategorie = new QTableWidgetItem( bdd.getValeur ( "tbl_fournisseur","nomFournisseur",
                                                                                "idFournisseur",
                                                                                listeFrs.at (i) ) );

        ui->tableWidget_fournisseur->setItem ( i, 0, itemId );
        itemId->setTextAlignment ( Qt::AlignCenter );
        ui->tableWidget_fournisseur->setItem ( i, 1, itemCategorie );
        itemCategorie->setTextAlignment ( Qt::AlignCenter);
    }
}















//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheFournisseur::envoyerMessage(QString message, bool alerte){

    emit on_message (message, alerte);
}












//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheFournisseur::on_btn_supprimer_clicked(){

    if( !ui->tableWidget_fournisseur->currentItem ()->text ().isEmpty () ){

        QString idFrs = ui->tableWidget_fournisseur->item ( ui->tableWidget_fournisseur->currentRow (),
                                                                headerTableau.indexOf ("ID") )->text ();

        if( bdd.compterEnregistrement( "tbl_stock", "idFournisseur", idFrs ) == 0 ){

            int reponse = QMessageBox::question (NULL,"Confirmaer suppression",
                                                 "Voulez-vous supprimer cette cat�gorie de produit ?",
                                                 QMessageBox::Yes | QMessageBox::No);

            if( reponse == QMessageBox::Yes ){

                if( bdd.supprimerDonnee ( "tbl_fournisseur", "idFournisseur", idFrs ) ){

                    ui->tableWidget_fournisseur->removeRow ( ui->tableWidget_fournisseur->currentRow () );
                    emit on_message ("Fournisseur supprim�", false);
                }
                else
                    emit on_message ("Fournisseur non supprim�", true);
            }
        }
        else
            emit on_message ("Impossible de supprimer ce fournisseur - Des produits existent sous ce fournisseur",
                             true);
    }
    else
        emit on_message ("Veuillez s�lectionner un fournisseur � supprimer", true);

}






//----------------------------------------------------------------------------------------------------------------------
void Form_rechercheFournisseur::on_btn_modifier_clicked(){


    Form_fournisseur *fournisseur = new Form_fournisseur("modifier");

    connect (fournisseur, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));
    connect (fournisseur, SIGNAL(categorieModifie()), this, SLOT(listerCategorie()));
    fournisseur->show ();
    fournisseur->chargerFournisseur ( ui->tableWidget_fournisseur->item ( ui->tableWidget_fournisseur->currentRow (),
                                                                    headerTableau.indexOf ("Fournisseur") )->text () );
}
