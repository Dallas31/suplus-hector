#ifndef FORM_STATSVENTE_H
#define FORM_STATSVENTE_H

#include <QWidget>
#include <QMessageBox>
#include <qcustomplot.h>

#include "database.h"


namespace Ui {
class Form_statsVente;
}

class Form_statsVente : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_statsVente(QWidget *parent = 0);
    ~Form_statsVente();

    void afficherGraphPoint(QVector<double> vecteurY, QVector<double> vecteurX);
    
private slots:
    void on_calendrier_clicked(const QDate &date);

    void on_checkBox_journee_clicked(bool checked);

    void on_checkBox_periode_clicked(bool checked);

    void on_btn_generer_clicked();

    void on_btn_exporter_clicked();

private:
    Ui::Form_statsVente *ui;

    int clicID;
};

#endif // FORM_STATSVENTE_H
