#include "form_preference.h"
#include "ui_form_preference.h"

Form_preference::Form_preference(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_preference)
{
    ui->setupUi(this);

    this->setWindowTitle ("Pr�f�rences");

    chargerDonnees ();
}

Form_preference::~Form_preference(){

    delete ui;
}



//----------------------------------------------------------------------------------------------------------------------
void Form_preference::chargerDonnees(){

    ui->tabWidget->setTabText (0,"E-mail");

    Configuration *config = new Configuration();

    ui->lineEdit_pathPostie->setText ( config->getValue ( NOM_FICHIER_CONFIG, "PATH","Postie") );
    ui->lineEdit_serveurMail->setText ( config->getValue ( NOM_FICHIER_CONFIG, "SMTP","Host") );
    ui->lineEdit_adrFrom->setText ( config->getValue ( NOM_FICHIER_CONFIG, "SMTP","from") );
    ui->lineEdit_adrTo->setText ( config->getValue ( NOM_FICHIER_CONFIG, "SMTP","to") );
}





//----------------------------------------------------------------------------------------------------------------------
void Form_preference::on_btn_parcourir_clicked(){


    // Ouverture de la bo�te de dialogue, modale
    QString pathPostie = QFileDialog::getOpenFileName(this, tr("Open File"),QCoreApplication::applicationDirPath (), tr("Ex�cutable (*.exe*)"));
    // On v�rifie que l'utilisateur a bien s�lectionn� un fichier
    // Si la cha�ne est vide, c'est que l'utilisateur a cliqu� sur annuler
    if(! pathPostie.isEmpty() )
        ui->lineEdit_pathPostie->setText ( pathPostie );

}
