#ifndef RAPPORT_H
#define RAPPORT_H

#include <QString>
#include <QStringList>
#include <QFile>
#include <QTextStream>
#include <QObject>
#include <QDate>

#include "database.h"

class Rapport : public QObject
{
    Q_OBJECT

public:
    Rapport();

    QStringList genererRapport();

signals:

    void on_rapportFini();

    void on_message(QString message, bool alerte);

private:

    Database bdd;
};

#endif // RAPPORT_H
