#include "form_rechercheproduits.h"
#include "ui_form_rechercheproduits.h"

Form_rechecheProduits::Form_rechecheProduits(QString codeBarre, QString quantite, QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_rechecheProduits)
{
    ui->setupUi(this);

    this->setWindowTitle ("Rechercher produit");

    ui->lineEdit_codeBarre->setAlignment ( Qt::AlignCenter );
    ui->lineEdit_quantite->setAlignment ( Qt::AlignCenter );

    ui->lineEdit_codeBarre->setText ( codeBarre );

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));


    headerTableau << "ID"
                  << "R�f�rence"
                  << "Produit"
                  << "Description"
                  << "Cat�gorie"
                  << "Quantit�"
                  << "Prix unitaire"
                  << "Prix fournisseur"
                  << "Fournisseur"
                  << "Alerte"
                  << "Quantit� alerte"
                  << "Solde"
                  << "Taux solde";

    ui->tableWidget_produits->setColumnCount ( headerTableau.length () );
    ui->tableWidget_produits->setHorizontalHeaderLabels ( headerTableau);

    if( contexte != "recherche" ){

        ui->btn_supprimer->setVisible ( true );
        ui->btn_supprimer->setEnabled ( false );

        ui->btn_modifier->setVisible ( true );
        ui->btn_modifier->setEnabled ( false );
    }
    else{

        ui->lineEdit_quantite->setText ( quantite );

        ui->btn_supprimer->setVisible ( false );
        ui->btn_modifier->setVisible ( false );
    }

    chargerIHM (codeBarre);
}

Form_rechecheProduits::~Form_rechecheProduits(){

    delete ui;
}















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::chargerIHM(QString reference){

    QStringList listeProduits = bdd.listerProduits( reference );

    ui->tableWidget_produits->setRowCount ( listeProduits.length () );

    for(int i = 0 ; i < listeProduits.length () ; i++ ){

        QStringList listeDetailsProduit = listeProduits.at (i).split (";");

        for(int j = 0 ; j < listeDetailsProduit.length () ; j++){

            if( !listeDetailsProduit.at (j).isEmpty () ){

                QTableWidgetItem *item;

                if( j == headerTableau.indexOf ("Cat�gorie") )
                    item = new QTableWidgetItem(
                                bdd.getValeur ("tbl_categorie","libelleCategorie", "idCategorie", listeDetailsProduit.at (j ) ) ) ;
                else if ( j == headerTableau.indexOf ("Fournisseur") )
                    item = new QTableWidgetItem(
                                bdd.getValeur ("tbl_fournisseur","nomFournisseur", "idFournisseur", listeDetailsProduit.at (j ) ) ) ;
                else
                    item = new QTableWidgetItem( listeDetailsProduit.at (j) ) ;

                item->setTextAlignment ( Qt::AlignCenter );

                ui->tableWidget_produits->setItem (i, j, item);
            }
        }
    }
}



















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_lineEdit_codeBarre_returnPressed(){

    chargerIHM ( ui->lineEdit_codeBarre->text () );
}





















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_btn_supprimer_clicked(){

    if( !ui->tableWidget_produits->currentItem ()->text ().isEmpty () ){

        int reponse = QMessageBox::question (NULL,"Confirmatio suppression","Souhaitez-vous supprimer ce produit ?",
                               QMessageBox::Yes | QMessageBox::No );

        if( reponse == QMessageBox::Yes ){

            QString ID = ui->tableWidget_produits->item ( ui->tableWidget_produits->currentRow (),
                                                           headerTableau.indexOf ("ID") )->text ();

            QString ref = ui->tableWidget_produits->item ( ui->tableWidget_produits->currentRow (),
                                                           headerTableau.indexOf ("R�f�rence") )->text ();

            if( bdd.supprimerDonnee ("tbl_stock", "idProduit", ID) ){

                QFile fichierCodeBarre;
                if( fichierCodeBarre.remove ( PATH_CODE_BARRE % "/" % ref % ".png" ) ){

                    QMessageBox::information (NULL,"Suppression produit","Produit supprim�");

                    ui->tableWidget_produits->removeRow ( ui->tableWidget_produits->currentRow () );

                    ui->lineEdit_quantite->clear ();
                    ui->lineEdit_codeBarre->clear ();
                }
                else
                    emit on_message ("Impossible de supprimer le code barre ( " % ref % " )", true);
            }
        }
    }
    else
        QMessageBox::warning (NULL,"S�lection produit","Veuillez s�lectionner un produit � supprimer");

}











//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_btn_modifier_clicked(){

    if( !ui->tableWidget_produits->currentItem ()->text ().isEmpty () ){

        QString idProduit = ui->tableWidget_produits->item ( ui->tableWidget_produits->currentRow (), headerTableau.indexOf ("ID") )->text ();

        Form_enregisterProduits *produit = new Form_enregisterProduits("modifier");
        connect (produit, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));
        produit->show ();

        QString reference = bdd.getValeur ("tbl_stock","reference","idProduit", idProduit );

        QString categorieProduit = bdd.getValeur ("tbl_categorie", "libelleCategorie", "idCategorie",
                                                  bdd.getValeur ("tbl_stock", "idCategorie", "idProduit", idProduit ) );

        QString fournisseurProduit = bdd.getValeur ("tbl_fournisseur", "nomFournisseur", "idFournisseur",
                                                  bdd.getValeur ("tbl_stock", "idFournisseur", "idProduit",  idProduit ) );

        QString nomProduit = bdd.getValeur ("tbl_stock","libelleProduit","idProduit", idProduit );
        QString pu = bdd.getValeur ("tbl_stock","prixUnitaire","idProduit", idProduit );
        QString prixFournisseur = bdd.getValeur ("tbl_stock","prixFournisseur","idProduit", idProduit );
        QString quantite = bdd.getValeur ("tbl_stock","quantiteProduit","idProduit", idProduit );
        QString descriptionProduit = bdd.getValeur ("tbl_stock","descriptionProduit","idProduit", idProduit );
        QString valeurAlerte = bdd.getValeur ("tbl_stock","alerte","idProduit", idProduit );
        QString quantiteMin = bdd.getValeur ("tbl_stock","quantiteAlerte","idProduit", idProduit );
        QString valeurSolde = bdd.getValeur ("tbl_stock","solder","idProduit", idProduit );
        QString tauxSolde = bdd.getValeur ("tbl_stock","tauxSolde","idProduit", idProduit );

        bool alerte = false;
        if( valeurAlerte == "oui" )
            alerte = true;

        bool solder = false;
        if( valeurSolde == "oui" )
            solder = true;

        produit->chargerProduit ( reference,
                                  categorieProduit,
                                  fournisseurProduit,
                                  nomProduit, pu, prixFournisseur, quantite, descriptionProduit,
                                   alerte, quantiteMin, solder, tauxSolde);
    }
    else
        QMessageBox::warning (NULL,"S�lection produit","Veuillez s�lectionner un produit � modifier");

}
















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_tableWidget_produits_cellDoubleClicked(int row, int column){

    int resultat = QMessageBox::question (NULL,"Confirmation","Souhaitez-vous s�lectionner ce produit ?", QMessageBox::Yes | QMessageBox::No );

    if( resultat == QMessageBox::Yes ){

        QString quantiteCommande = ui->lineEdit_quantite->text ();

        if ( quantiteCommande.toInt () >ui->tableWidget_produits->item ( row, headerTableau.indexOf ("Quantit�") )->text ().toInt ()  )
            emit on_message ("Quantit� demand�e trop �lev�", true);

        else{

            QString pu = ui->tableWidget_produits->item ( row, headerTableau.indexOf ("Prix unitaire") )->text ();
            QStringList listeProduit;

            QString detailProduit =

                    ui->tableWidget_produits->item ( row, headerTableau.indexOf ("ID") )->text ()
                    % ";"
                    % ui->tableWidget_produits->item ( row, headerTableau.indexOf ("Produit") )->text ()
                    % ";"
                    % ui->tableWidget_produits->item ( row, headerTableau.indexOf ("Description") )->text ()
                    % ";"
                    % pu
                    % ";"
                    % quantiteCommande
                    % ";"
                    % QString::number ( pu.toFloat ()* quantiteCommande.toInt () );

            listeProduit.push_back ( detailProduit  );

            emit produitValider ( listeProduit );
        }
    }
    close();
}















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::envoyerMessage (QString message, bool alerte){

    emit on_message ( message, alerte );
}
















//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_tableWidget_produits_itemClicked(QTableWidgetItem *item){

    ui->btn_modifier->setEnabled ( true );
    ui->btn_supprimer->setEnabled ( true );
}

























//----------------------------------------------------------------------------------------------------------------------
void Form_rechecheProduits::on_checkBox_afficherStock_clicked(bool checked){

    if( checked )
        chargerIHM ( "-1" );

    else
        ui->tableWidget_produits->setRowCount (0);

}
