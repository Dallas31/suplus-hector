#include "form_client.h"
#include "ui_form_client.h"

Form_client::Form_client(QString contexte, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form_client)
{
    ui->setupUi(this);
    this->setWindowTitle ("Ajout client");

    if( contexte == "ajout" )
        ui->btn_modifierClient->setVisible ( false );
    else{

        ui->btn_modifierClient->setVisible ( true );
        ui->btn_enregistrerClient->setVisible ( false );
    }

    connect (&bdd, SIGNAL(on_message(QString,bool)), this, SLOT(envoyerMessage(QString,bool)));

    ui->comboBox_typeClient->addItems ( bdd.listerValeur ("tbl_typeClient" ,"libelleTypeClient") );

    ui->radioButton_sexeM->setChecked ( true );
    sexeClient = "M";
}

Form_client::~Form_client()
{
    delete ui;
}






//----------------------------------------------------------------------------------------------------------------------
void Form_client::viderIHM(){

    ui->lineEdit_nomClient->clear ();
    ui->lineEdit_prenomClient->clear ();
    ui->lineEdit_age->clear ();
    ui->textEdit_adresse->clear ();
    ui->lineEdit_dateNaissance->clear ();
    ui->lineEdit_mail->clear ();
    ui->lineEdit_telephone->clear ();
}












//----------------------------------------------------------------------------------------------------------------------
void Form_client::chargerClient(QString idClientSelect, QString typeClient, QString nomClient, QString prenomClient,
                                QString sexe, QString age, QString adresse, QString dateNaissance, QString email, QString tel){

    idClient = idClientSelect;

    ui->comboBox_typeClient->setCurrentIndex ( ui->comboBox_typeClient->findText ( typeClient ) );
    ui->lineEdit_nomClient->setText ( nomClient );
    ui->lineEdit_prenomClient->setText ( prenomClient );

    if( sexe == "M" )
        ui->radioButton_sexeM->setChecked ( true );
    else
        ui->radioButton_sexeF->setChecked ( true );

    ui->lineEdit_age->setText ( age );

    ui->lineEdit_dateNaissance->setText ( dateNaissance );
    ui->lineEdit_mail->setText ( email );
    ui->lineEdit_telephone->setText ( tel );

    ui->textEdit_adresse->setText ( adresse );
}






//----------------------------------------------------------------------------------------------------------------------
void Form_client::on_btn_quitter_clicked(){

    close ();
}








//----------------------------------------------------------------------------------------------------------------------
void Form_client::keyPressEvent(QKeyEvent *event){

    if( event->key () == Qt::Key_Enter || event->key () == Qt::Key_Return )
        on_btn_enregistrerClient_clicked ();

    else if( event->key () == Qt::Key_Escape )
        on_btn_quitter_clicked ();

}









//----------------------------------------------------------------------------------------------------------------------
void Form_client::envoyerMessage (QString message, bool alerte){

    emit on_message ( message, alerte );
}








//----------------------------------------------------------------------------------------------------------------------
void Form_client::on_btn_enregistrerClient_clicked(){

    if( ui->radioButton_sexeF->isChecked () )
        sexeClient = "F";

    QStringList listeValeurFormulaire;
    listeValeurFormulaire.push_back ( ui->lineEdit_nomClient->text () );
    listeValeurFormulaire.push_back ( ui->lineEdit_prenomClient->text () );
    listeValeurFormulaire.push_back ( ui->lineEdit_age->text () );
    listeValeurFormulaire.push_back ( ui->textEdit_adresse->toPlainText () );
    listeValeurFormulaire.push_back ( ui->lineEdit_dateNaissance->text () );
    listeValeurFormulaire.push_back ( ui->lineEdit_telephone->text () );
    listeValeurFormulaire.push_back ( ui->lineEdit_mail->text () );

    bool annuler = false;

    for( int i = 0 ; i < listeValeurFormulaire.length () ; i++ ){

        if( listeValeurFormulaire.at (i).isEmpty () ){

            emit on_message ("Veuillez remplir correctement le formulaire", true);
            annuler = true;
            break;
        }
    }

    if( !annuler ){

        if( bdd.ajouterClient ( ui->lineEdit_nomClient->text (),
                                ui->lineEdit_prenomClient->text (),
                                sexeClient,
                                ui->lineEdit_dateNaissance->text (),
                                ui->lineEdit_mail->text (),
                                ui->textEdit_adresse->toPlainText (),
                                ui->lineEdit_telephone->text (),
                                ui->lineEdit_age->text (), "0",
                                bdd.getValeur ( "tbl_typeClient", "idTypeClient", "libelleTypeClient", ui->comboBox_typeClient->currentText () ) ) )
            emit on_message ("Client ajout� en base de donn�es", false);

        viderIHM();

    }
}







//----------------------------------------------------------------------------------------------------------------------
void Form_client::on_btn_modifierClient_clicked(){

    QString sexeClient = "M";
    if( ui->radioButton_sexeF->isChecked () )
        sexeClient = "F";

    bdd.majDonnee ("tbl_client", "nomClient", ui->lineEdit_nomClient->text (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "prenomClient", ui->lineEdit_prenomClient->text (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "ageClient", ui->lineEdit_age->text (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "adresseClient", ui->textEdit_adresse->toPlainText (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "sexe", sexeClient, "idClient", idClient);
    bdd.majDonnee ("tbl_client", "dateNaissance", ui->lineEdit_dateNaissance->text (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "adresseMail",  ui->lineEdit_mail->text (), "idClient", idClient);
    bdd.majDonnee ("tbl_client", "telephone", ui->lineEdit_telephone->text (), "idClient", idClient);

    bdd.majDonnee ("tbl_client", "idTypeClient",
                   bdd.getValeur ("tbl_typeClient", "idTypeClient", "libelleTypeClient", ui->comboBox_typeClient->currentText () )
                   , "idClient", idClient);

    emit on_message ("Client mis � jour", false);

}
