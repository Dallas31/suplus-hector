#ifndef FORM_ENREGISTERPRODUITS_H
#define FORM_ENREGISTERPRODUITS_H

#include <QWidget>
#include <QStringBuilder>
#include <QString>
#include <QStringList>
#include <QMessageBox>
#include <QStringBuilder>
#include <QKeyEvent>
#include <QProcess>
#include <windows.h>
#include <stdlib.h>

#include "database.h"


#define PATH_ZINT          QString(QCoreApplication::applicationDirPath () % "/Zint/zint.exe")
#define PATH_CODE_BARRE    QString(QCoreApplication::applicationDirPath () % "/Code_barre_produit")

namespace Ui {
class Form_enregisterProduits;
}

class Form_enregisterProduits : public QWidget
{
    Q_OBJECT
    
public:
    explicit Form_enregisterProduits(QString contexte, QWidget *parent = 0);
    ~Form_enregisterProduits();

    void chargerProduit(QString reference, QString categorie, QString fournisseur, QString libelle, QString pu,
                        QString prixFournisseur, QString quantite, QString desc, bool alerte, QString quantiteAlert,
                        bool solde, QString tauxSolde);

private:

    void viderIHM();
    
private slots:
    void on_btn_enregistrerProduit_clicked();

    void envoyerMessage(QString message, bool alerte);

    void on_checkBox_alerte_clicked(bool checked);

    void keyPressEvent(QKeyEvent *event);

    void on_btn_modifierProduit_clicked();

    void on_checkBox_solde_clicked(bool checked);

    void on_btn_genererCodeBarre_clicked();

signals:

    void on_message(QString message, bool alerte);

private:
    Ui::Form_enregisterProduits *ui;

    Database bdd;
};

#endif // FORM_ENREGISTERPRODUITS_H
